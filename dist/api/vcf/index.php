<?php
error_reporting(-1);
ini_set('display_errors', 'On');
require_once "DBController.php";
$dbController = new DBController();



if(!empty($_GET["action"])) {
    $query = "SELECT * FROM am_contacts WHERE id = ?";
    $param_type = "i";
    $param_value_array = array($_GET["id"]);
    $contactResult = $dbController->runQuery($query,$param_type,$param_value_array);
    
    require_once "VcardExportV2.php";
    $vcardExport = new VcardExport();
    $vcardExport->contactVcardExportService($contactResult);
    exit;
}

$query = "SELECT * FROM am_contacts";
$result = $dbController->runBaseQuery($query);
?>
<!DOCTYPE html>
<html>
<head>
<title>Magiktap Portal</title>
<link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
<?php 
if(!empty($result))
{
?>
    <div class="tbl-contact">
        <div class="contact-row-header">
            <div class="col_name">Name</div>
            <div>Email</div>
            <div>Phone</div>
            <div>Address</div>
            <div>Url</div>
            <div class="action">Export</div>
        </div>
<?php 
    foreach($result as $k=>$v)
    {
?>
        
        
        <div class="contact-row">
            <div class="col_name"><?php echo $result[$k]["Name"]; ?></div>
            <div><?php echo $result[$k]["Email_1_Value"]; ?></div>
            <div><?php echo $result[$k]["Phone_1_Value"]; ?></div>
            <div><?php echo $result[$k]["Address_1_Formatted"]; ?></div>
            <div><?php echo $result[$k]["Website_1_Type"]; ?></div>
            <div class="action"><a href="index.php?action=export&id=<?php echo $result[$k]["id"]; ?>" title="Export to vCard"><img src="vcard_icon.png" alt="vCard"></a></div>
        </div>
<?php 
    }
?>
    </div>
<?php 
}
?>
</body>
</html>
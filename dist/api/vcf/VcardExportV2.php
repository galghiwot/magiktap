<?php
use JeroenDesloovere\VCard\VCard;

class VcardExport
{

    public function contactVcardExportService($contactResult)
    {
        /*require_once 'vendor/Behat-Transliterator/Transliterator.php';
        require_once 'vendor/jeroendesloovere-vcard/VCard.php';
        // define vcard
        $vcardObj = new VCard();
        $lastname = $contactResult[0]["last_name"];
        $firstname = $contactResult[0]["first_name"];
        $additional = '';
        $prefix = '';
        $suffix = '';
        
        // add personal data
        $vcard->addName($lastname, $firstname, $additional, $prefix, $suffix);
        
        // add work data
        $vcard->addCompany('Company');
        $vcard->addJobtitle('Job Title');
        $vcard->addRole('Role');
        $vcard->addEmail($contactResult[0]["email_1"], 'PREF;WORK');
        $vcard->addPhoneNumber($contactResult[0]["phone_1"], 'PREF;WORK');
        $vcard->addPhoneNumber($contactResult[0]["phone_2"], 'WORK');
        $vcard->addAddress($contactResult[0]["address_1"], $contactResult[0]["address_2"], $contactResult[0]["address_1"], $contactResult[0]["city"], null, $contactResult[0]["zipcode"], 'USA','POSTAL');
        $vcard->addURL($contactResult[0]["url"]);
        $vcard->addPhoto(__DIR__ . '/user.png');
        return $vcardObj->download();
        */
        require_once 'vendor/Behat-Transliterator/Transliterator.php';
        //require_once 'vendor/jeroendesloovere-vcard/VCard.php';
        require_once 'vendor/vcard-master/VCard.php';
        // define vcard
        $vcard = new VCard();
        $lastname = $contactResult[0]["Given_Name"];
        $firstname = $contactResult[0]["Family_Name"];
        $additional = '';
        $prefix = '';
        $suffix = '';
        
        // add personal data
        $vcard->addName($lastname, $firstname, $additional, $prefix, $suffix);
        // add personal data
        // $vcardObj->addName($contactResult[0]["first_name"] . " " . $contactResult[0]["last_name"]);
        // $vcardObj->addBirthday($contactResult[0]["01/01/2000"]);
        // $vcardObj->addEmail($contactResult[0]["email_1"]);
        // $vcardObj->addPhoneNumber($contactResult[0]["phone_1"]);
        // $vcardObj->addAddress($contactResult[0]["address_1"]);
        $vcard->addCompany($contactResult[0]["Organization_1_Name"]);
        $vcard->addJobtitle($contactResult[0]["Organization_1_Title"]);
        $vcard->addRole($contactResult[0]["Organization_1_Title"]);
        $vcard->addEmail($contactResult[0]["Email_1_Value"], 'PREF;WORK');
        $vcard->addPhoneNumber($contactResult[0]["Phone_1_Value"], 'PREF;WORK');
        $vcard->addPhoneNumber($contactResult[0]["Phone_2_Value"], 'WORK');
        $vcard->addAddress($contactResult[0]["Address_1_Street"], $contactResult[0]["Address_1_Street"], $contactResult[0]["Address_1_Street"], $contactResult[0]["Address_1_City"], null, $contactResult[0]["Address_1_Postal_Code"], 'USA','POSTAL');
        $vcard->addURL($contactResult[0]["Website_1_Value"]);
        //$vcard->addPhoto('https://via.placeholder.com/150');
        return $vcard->download();
    }
}

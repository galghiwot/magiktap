<?php 
    require_once '../../Classes/Organization.php';
    require_once '../../Classes/Session.php';

    $organizations = new Organization();

    $roles = $organizations->getRoles();    
    $statuss = $organizations->getStatuss();
	// headers to tell that result is JSON
	header('Content-type: application/json');
    echo json_encode(array('success'=>true,"roles"=> $roles, "statuss" => $statuss));
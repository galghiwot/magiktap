<?php 
    require_once '../../Classes/Organization.php';
    require_once '../../Classes/Session.php';

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata, true);        // Convert from object to array

    $session = Session::getInstance();
    /*if(! $session->id) {
        echo json_encode(array('error'=> 'You are not authorised to access this page.'));
        die();
    }*/

    //$request['createdBy'] = $session->id;       // Add session id to request 

    $organization = new Organization();
    $result = $organization->addOrganization($request);
    if($result){
		// headers to tell that result is JSON
		header('Content-type: application/json');
        echo json_encode(array('success'=>true,'message'=> "The organization details has been added to database.",'data'=>$result));
    }else{
		// headers to tell that result is JSON
		header('Content-type: application/json');
		echo json_encode(array('success'=>false, 'message'=> "The organization details cannot be added in database."));
	}
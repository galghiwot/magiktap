<?php 
    require_once '../../Classes/Organization.php';
    require_once '../../Classes/Session.php';

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata, true);        // Convert from object to array

    $session = Session::getInstance();
    if(! $session->id) {
        echo json_encode(array('error'=> 'You are not authorised to access this page.'));
        die();
    }

    $organizations = new Organization();
    if($organizations->deleteOrganization($request)){
		// headers to tell that result is JSON
		header('Content-type: application/json');
        echo json_encode(array('success'=>true,'message'=> "The organization has been deleted from database."));
    }else{
		// headers to tell that result is JSON
		header('Content-type: application/json');
        echo json_encode(array('success'=>false,'message'=>"The organization details cannot be found in database."));
	}
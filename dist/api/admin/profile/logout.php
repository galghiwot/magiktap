<?php 
    require_once '../../Classes/Session.php';

    $session = Session::getInstance();
    $session->destroy();
	// headers to tell that result is JSON
	header('Content-type: application/json');
    echo json_encode(array('success'=>true,"message"=>"You have been looged out successfully !"));
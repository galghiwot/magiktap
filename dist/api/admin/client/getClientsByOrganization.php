<?php 
require_once '../../Classes/Address.php';
    require_once '../../Classes/EmailAddress.php';
    require_once '../../Classes/Name.php';
    require_once '../../Classes/Phone.php';
    require_once '../../Classes/WebUrl.php';
    require_once '../../Classes/Social.php';
    require_once '../../Classes/Organization.php';
    require_once '../../Classes/CustomField.php';
    require_once '../../Classes/Session.php';

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata, true);        // Convert from object to array

    $session = Session::getInstance();
    if(! $session->id) {
        echo json_encode(array('error'=> 'You are not authorised to access this page.'));
        die();
    }
	// headers to tell that result is JSON
	header('Content-type: application/json');

    $names = new Name();
    $nameData = $names->getNamesByOrganization($request);
    $data = array();
    foreach ($nameData as $value) {
        $phones = new Phone();
        $phoneData = $phones->getPhonesByContact($value['id']);

        $orgs = new Organization();
        $orgData = $orgs->getOrganization($value['organization_id']);

        $addresses = new Address();
        $addressData = $addresses->getAddressesByContact($value['id']);
        
        $emailAddresses = new EmailAddress();
        $emailAddressData = $emailAddresses->getEmailAddressesByContact($value['id']);

        $webUrls = new WebUrl();
        $webUrlData = $webUrls->getWebUrlsByContact($value['id']);

        $CustomFields = new CustomField();
        $customData = $CustomFields->getCustomFieldsByContact($value['id']);

        
        $socials = new Social();
        $socialData = $socials->getSocialsByContact($value['id']);
        array_push($data, array("names"=>$value,"organization"=>$orgData,"addresses"=>$addressData,
        "phones"=>$phoneData,"emailAddresses"=>$emailAddressData,"webUrls"=>$webUrlData,
        "customFields"=>$customData,"socials"=>$socialData));
    }
    echo json_encode(array('success'=>true,"data"=>$data));
<?php 
    require_once '../../Classes/Contact.php';
    require_once '../../Classes/Session.php';

    $contacts = new Contact();

    $roles = $contacts->getRoles();    
    $statuss = $contacts->getStatuss();
	// headers to tell that result is JSON
	header('Content-type: application/json');
    echo json_encode(array('success'=>true,"roles"=> $roles, "statuss" => $statuss));
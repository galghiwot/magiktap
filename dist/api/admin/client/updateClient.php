<?php 
    require_once '../../Classes/Address.php';
    require_once '../../Classes/EmailAddress.php';
    require_once '../../Classes/Name.php';
    require_once '../../Classes/Phone.php';
    require_once '../../Classes/WebUrl.php';
    require_once '../../Classes/CustomField.php';
    require_once '../../Classes/Social.php';
    require_once '../../Classes/Session.php';

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata, true);        // Convert from object to array

    //$session = Session::getInstance();
    //if(! $session->id) {
    //    echo json_encode(array('error'=> 'You are not authorised to access this page.'));
    //    die();
   // }
    //Update Names
    $names = new Name();
    $nameAdded = 9999;
    $requestName = $request['names'];
    if(isset($requestName['id'])){
        $result  = $names->updateName($requestName, $requestName['id']);
        $client_id= $requestName['id'];
    }else{
        $client_id  = $names->addName($requestName);
    }
    
    //Add or Update Phones
    foreach ($request['phones'] as $phoneValue) {
        $phones = new Phone();
        if(isset($phoneValue['id'])){
            $resultPhone  = $phones->updatePhone($phoneValue, $phoneValue['id']);
        }else{
            $phoneValue['client_id'] = $client_id;
            $resultPhone  = $phones->addPhone($phoneValue);
        }
    }

    //Add or Update EmailAddresses
    foreach ($request['emailAddresses'] as $emailAddresseValue) {
        $emailAddresses = new EmailAddress();
        if(isset($emailAddresseValue['id'])){
            $resultEmailAddresse  = $emailAddresses->updateEmailAddress($emailAddresseValue, $emailAddresseValue['id']);
        }else{
            $emailAddresseValue['client_id'] = $client_id;
            $resultEmailAddresse  = $emailAddresses->addEmailAddress($emailAddresseValue);
        }
    }
  
    //Add or Update webUrls
  if(isset($request['webUrls'])){  
    foreach ($request['webUrls'] as $webUrlValue) {
        $webUrls = new WebUrl();
        if(isset($webUrlValue['id'])){
            $resultWebUrl  = $webUrls->updateWebUrl($webUrlValue, $webUrlValue['id']);
        }else{
            $webUrlValue['client_id'] = $client_id;
            $resultWebUrl  = $webUrls->addWebUrl($webUrlValue);
        }
    }
  }
     //Add or Update Address
     if(isset($request['addresses'])){  
        foreach ($request['addresses'] as $addressValue) {
            $addresses = new Address();
            if(isset($addressValue['id'])){
                $resultAddress  = $addresses->updateAddress($addressValue, $addressValue['id']);
            }else{
                $addressValue['client_id'] = $client_id;
                $resultAddress  = $addresses->addAddress($addressValue);
            }
        }
    }

    //Add or Update Custom Fields
     if(isset($request['customFields'])){  
        foreach ($request['customFields'] as $customFieldValue) {
            $customFields = new CustomField();
            if(isset($customFieldValue['id'])){
                $resultCustomField  = $customFields->updateCustomField($customFieldValue, $customFieldValue['id']);
            }else{
                $customFieldValue['client_id'] = $client_id;
                $resultCustomField  = $customFields->addCustomField($customFieldValue);
            }
        }
     }
    //$data = array();
   /* foreach ($nameData as $value) {
        $phones = new Phone();
        $phoneData = $phones->getPhonesByContact($value['id']);

        $addresses = new Address();
        $addressData = $addresses->getAddressesByContact($value['id']);
        
        $emailAddresses = new EmailAddress();
        $emailAddressData = $emailAddresses->getEmailAddressesByContact($value['id']);

        $webUrls = new WebUrl();
        $webUrlData = $webUrls->getWebUrlsByContact($value['id']);

        $socials = new Social();
        $socialData = $socials->getSocialsByContact($value['id']);
        //array_push($data, array("names"=>$value,"addresses"=>$addressData,"phones"=>$phoneData,"emailAddresses"=>$emailAddressData,"webUrls"=>$webUrlData,"socials"=>$socialData));
    }*/
    //echo json_encode(array('success'=>true,"data"=>$data));

    if($client_id){
		// headers to tell that result is JSON
		header('Content-type: application/json');
        echo json_encode(array('success'=>true,"message"=> "The client details has been updated.",'data'=>$client_id));
	}
    else{
		// headers to tell that result is JSON
		header('Content-type: application/json');
        echo json_encode(array('success'=>false,"message"=> "The client details cannot be updated in database."));
	}
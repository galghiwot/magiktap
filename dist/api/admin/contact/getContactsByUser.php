<?php 
    require_once '../../Classes/Contact.php';
    require_once '../../Classes/Session.php';

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata, true);        // Convert from object to array

    $session = Session::getInstance();
    if(! $session->id) {
        echo json_encode(array('error'=> 'You are not authorised to access this page.'));
        die();
    }
	// headers to tell that result is JSON
	header('Content-type: application/json');
    $contacts = new Contact();
    $contactData = $contacts->getContactsByUser($request);
    //echo json_encode($contactData);
	echo json_encode(array('success'=>true,"data"=>$contactData));
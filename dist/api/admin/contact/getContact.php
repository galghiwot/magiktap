<?php 
    require_once '../../Classes/Address.php';
    require_once '../../Classes/EmailAddress.php';
    require_once '../../Classes/Name.php';
    require_once '../../Classes/Phone.php';
    require_once '../../Classes/WebUrl.php';
    require_once '../../Classes/Social.php';
    require_once '../../Classes/Session.php';

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata, true);        // Convert from object to array

    $session = Session::getInstance();
    /*if(! $session->id) {
        echo json_encode(array('error'=> 'You are not authorised to access this page.'));
        die();
    }*/
	// headers to tell that result is JSON
	header('Content-type: application/json');
    $contacts = new Contact();
    $contactData = $contacts->getContact($request);
    
    $names = new Name();
    $nameData = $names->getName($request);

    $phones = new Phone();
    $phoneData = $phones->getPhonesByContact($request);

    $addresses = new Address();
    $addressData = $addresses->getAddressesByContact($request);
    
    $emailAddresses = new EmailAddress();
    $emailAddressData = $emailAddresses->getEmailAddressesByContact($request);

    $webUrls = new WebUrl();
    $webUrlData = $webUrls->getWebUrlsByContact($request);

    $socials = new Social();
    $socialData = $socials->getSocialsByContact($request);

    //echo json_encode($contactData);
	echo json_encode(array('success'=>true,"data"=>$contactData,"names"=>$nameData,"names"=>$nameData,"addresses"=>$addressData,"phones"=>$phoneData,"emailAddresses"=>$emailAddressData,"webUrls"=>$webUrlData,"socials"=>$socialData)); 
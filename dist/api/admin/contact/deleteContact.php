<?php 
    require_once '../../Classes/Contact.php';
    require_once '../../Classes/Session.php';

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata, true);        // Convert from object to array

    $session = Session::getInstance();
    if(! $session->id) {
        echo json_encode(array('error'=> 'You are not authorised to access this page.'));
        die();
    }

    $contacts = new Contact();
    if($contacts->deleteContact($request)){
		// headers to tell that result is JSON
		header('Content-type: application/json');
        echo json_encode(array('success'=>true,'message'=> "The contact has been deleted from database."));
    }else{
		// headers to tell that result is JSON
		header('Content-type: application/json');
        echo json_encode(array('success'=>false,'message'=>"The contact details cannot be found in database."));
	}
<?php 
    require_once '../../Classes/Contact.php';
    require_once '../../Classes/Session.php';

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata, true);        // Convert from object to array

    $session = Session::getInstance();
    /*if(! $session->id) {
        echo json_encode(array('error'=> 'You are not authorised to access this page.'));
        die();
    }*/

    //$request['createdBy'] = $session->id;       // Add session id to request 

    $contact = new Contact();
    $result = $contact->addContact($request);
    if($result){
		// headers to tell that result is JSON
		header('Content-type: application/json');
        echo json_encode(array('success'=>true,'message'=> "The contact details has been added to database.",'data'=>$result));
    }else{
		// headers to tell that result is JSON
		header('Content-type: application/json');
		echo json_encode(array('success'=>false, 'message'=> "The contact details cannot be added in database."));
	}
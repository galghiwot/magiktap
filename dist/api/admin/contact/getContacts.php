<?php 
    require_once '../../Classes/Contact.php';
    require_once '../../Classes/Session.php';

    $session = Session::getInstance();
    if(! $session->id) {
        echo json_encode(array('error'=> 'You are not authorised to access this page.'));
        die();
    }
	// headers to tell that result is JSON
	header('Content-type: application/json');

    $contacts = new Contact();
    $allContacts = $contacts->getContacts();
    echo json_encode(array('success'=>true,"data"=>$allContacts));
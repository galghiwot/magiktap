<?php 
    require_once '../../Classes/Card.php';
    require_once '../../Classes/Session.php';

    $cards = new Card();

    $roles = $cards->getRoles();    
    $statuss = $cards->getStatuss();
	// headers to tell that result is JSON
	header('Content-type: application/json');
    echo json_encode(array('success'=>true,"roles"=> $roles, "statuss" => $statuss));
<?php 
    require_once '../../Classes/Card.php';
    require_once '../../Classes/Session.php';

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata, true);        // Convert from object to array

    $session = Session::getInstance();
    /*if(! $session->id) {
        echo json_encode(array('error'=> 'You are not authorised to access this page.'));
        die();
    }*/

    $card = new Card();
    $result = $card->updateCard($request, $request['id']);
    if($result){
		// headers to tell that result is JSON
		header('Content-type: application/json');
        echo json_encode(array('success'=>true,"message"=> "The card details has been updated.",'data'=>$result));
	}
    else{
		// headers to tell that result is JSON
		header('Content-type: application/json');
        echo json_encode(array('success'=>false,"message"=> "The card details cannot be updated in database."));
	}
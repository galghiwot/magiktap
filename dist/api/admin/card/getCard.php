<?php 
    require_once '../../Classes/Card.php';
    require_once '../../Classes/Session.php';

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata, true);        // Convert from object to array

    $session = Session::getInstance();
    /*if(! $session->id) {
        echo json_encode(array('error'=> 'You are not authorised to access this page.'));
        die();
    }*/
	// headers to tell that result is JSON
	header('Content-type: application/json');
    $cards = new Card();
    $cardData = $cards->getCard($request);
    //echo json_encode($cardData);
	echo json_encode(array('success'=>true,"data"=>$cardData==false? null:$cardData));
<?php 
    require_once '../../Classes/Card.php';
    require_once '../../Classes/Session.php';

    $session = Session::getInstance();
    if(! $session->id) {
        echo json_encode(array('error'=> 'You are not authorised to access this page.'));
        die();
    }
	// headers to tell that result is JSON
	header('Content-type: application/json');

    $cards = new Card();
    $allCards = $cards->getCards();
    echo json_encode(array('success'=>true,"data"=>$allCards));
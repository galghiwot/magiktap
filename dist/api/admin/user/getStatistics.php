<?php 
    require_once '../../Classes/User.php';
    require_once '../../Classes/Session.php';

    $session = Session::getInstance();
    if(! $session->id) {
        echo json_encode(array('error'=> 'You are not authorised to access this page.'));
        die();
    }

    $user = new User();
    $result = $user->getStatistics();

    if($result) {
		// headers to tell that result is JSON
		header('Content-type: application/json');
        echo json_encode(array('success'=>true,"data"=> $result));
    }
    else{
		// headers to tell that result is JSON
		header('Content-type: application/json');
        echo json_encode(array('success'=>false,"data"=> "The details cannot be accessed at this moment. Please try again later."));
	}
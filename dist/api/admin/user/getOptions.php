<?php 
    require_once '../../Classes/User.php';
    require_once '../../Classes/Session.php';

    $users = new User();

    $roles = $users->getRoles();    
    $statuss = $users->getStatuss();
	// headers to tell that result is JSON
	header('Content-type: application/json');
    echo json_encode(array('success'=>true,"roles"=> $roles, "statuss" => $statuss));
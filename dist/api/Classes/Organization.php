<?php
require_once 'Database.php';
const ORGANIZATIONS_TABLE = "am_organization";
const HASH_SALT = "Rand0m!S@lt";
const SITE_NAME = "MagikTap";

class Organization {
    private $pdo;

    function __construct() 
    {
        $dbInstance = Database::getInstance();
        $this->pdo = $dbInstance->getConnection(); 
    }

    public function getOrganizations()
    {
        $sql = "SELECT * FROM " . ORGANIZATIONS_TABLE;
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getOrganization($id)
    {
        $sql = "SELECT * FROM " . ORGANIZATIONS_TABLE . " WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result;
    }

    public function getOrganizationsByUser($id)
    {
        $sql = "SELECT * FROM " . ORGANIZATIONS_TABLE . " WHERE super_user_id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getOrganizationsByParentId($id){
        $sql = "SELECT * FROM " . ORGANIZATIONS_TABLE . " WHERE parent_id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function addOrganization($organizationData)
    {
        $keys = implode(",", array_keys($organizationData));
        $questionMarks = Database::placeholders($organizationData);
        
        $sql = "INSERT INTO " . ORGANIZATIONS_TABLE . " ($keys) VALUES ($questionMarks)";
        $stmt = $this->pdo->prepare($sql);

        $count=1;
        //$organizationData['pwd'] = $this->hashPassword($organizationData['pwd']);
        foreach ($organizationData as $value) {
            $stmt->bindValue($count++, $value);
        }

        $stmt->execute();
        return $this->pdo->lastInsertId();    
    }

    public function updateOrganization($organizationData, $id)
    {
        /* Check for duplicate email address */
       /* $sql = "SELECT * FROM " . ORGANIZATIONS_TABLE . " WHERE email = ? OR username = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $organizationData['email']);
        $stmt->bindValue(2, $organizationData['username']);
        $stmt->execute();
        if($stmt->fetch())  
            return null;*/

        /** Remove id from the organizationData array if present   otherwise it will be inserted in UPDATE SQL Query **/
        if(!isset($organizationData['id']))
            unset($organizationData['id']);                             
    
        $setValues = Database::setValues($organizationData);             // Build query string

        $sql = "UPDATE " . ORGANIZATIONS_TABLE . " SET $setValues WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
    
        $count=1;
        foreach ($organizationData as $value) {
            $stmt->bindValue($count++, $value);
        }
        $stmt->bindValue($count, $id);
    
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function deleteOrganization($id)
    {
        $sql = "UPDATE " . ORGANIZATIONS_TABLE . " SET Status=? WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, "Deleted");
        $stmt->bindValue(2, $id);
       
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function hardDeleteOrganization($id)
    {
        $sql = "DELETE FROM " . ORGANIZATIONS_TABLE . " WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
       
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function getStatuss() 
    {
        $sql = "SELECT status FROM " . ORGANIZATIONS_TABLE . " GROUP BY Status";
        $stmt = $this->pdo->prepare($sql);
       
        $stmt->execute();
        return $stmt->fetchAll();
    }
 
}

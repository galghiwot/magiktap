<?php
require_once 'Database.php';
const CARDS_TABLE = "am_cards";
const HASH_SALT = "Rand0m!S@lt";
const SITE_NAME = "MagikTap";

class Card {
    private $pdo;

    function __construct() 
    {
        $dbInstance = Database::getInstance();
        $this->pdo = $dbInstance->getConnection(); 
    }

    public function getCards()
    {
        $sql = "SELECT * FROM " . CARDS_TABLE;
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getCard($id)
    {
        $sql = "SELECT * FROM " . CARDS_TABLE . " WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result;
    }

    public function getCardsByContact($id)
    {
        $sql = "SELECT * FROM " . CARDS_TABLE . " WHERE Contact_id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function addCard($cardData)
    {
        $keys = implode(",", array_keys($cardData));
        $questionMarks = Database::placeholders($cardData);
        
        $sql = "INSERT INTO " . CARDS_TABLE . " ($keys) VALUES ($questionMarks)";
        $stmt = $this->pdo->prepare($sql);

        $count=1;
        //$cardData['pwd'] = $this->hashPassword($cardData['pwd']);
        foreach ($cardData as $value) {
            $stmt->bindValue($count++, $value);
        }

        $stmt->execute();
        return $this->pdo->lastInsertId();    
    }

    public function updateCard($cardData, $id)
    {
        /* Check for duplicate email address */
       /* $sql = "SELECT * FROM " . CARDS_TABLE . " WHERE email = ? OR username = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $cardData['email']);
        $stmt->bindValue(2, $cardData['username']);
        $stmt->execute();
        if($stmt->fetch())  
            return null;*/

        /** Remove id from the cardData array if present    otherwise it will be inserted in UPDATE SQL Query **/
        if(!isset($cardData['id']))
            unset($cardData['id']);                             
    
        $setValues = Database::setValues($cardData);             // Build query string

        $sql = "UPDATE " . CARDS_TABLE . " SET $setValues WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
    
        $count=1;
        foreach ($cardData as $value) {
            $stmt->bindValue($count++, $value);
        }
        $stmt->bindValue($count, $id);
    
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function deleteCard($id)
    {
        $sql = "UPDATE " . CARDS_TABLE . " SET Status=? WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, "Deleted");
        $stmt->bindValue(2, $id);
       
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function hardDeleteCard($id)
    {
        $sql = "DELETE FROM " . CARDS_TABLE . " WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
       
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function getStatuss() 
    {
        $sql = "SELECT status FROM " . CARDS_TABLE . " GROUP BY Status";
        $stmt = $this->pdo->prepare($sql);
       
        $stmt->execute();
        return $stmt->fetchAll();
    }
 
}
/*
Card Array Strucutre
id
card_id
status
*/
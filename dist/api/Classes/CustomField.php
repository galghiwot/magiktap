<?php
require_once 'Database.php';
const CUSTOM_TABLE = "am_custom_field_info";

class CustomField {
    private $pdo;

    function __construct() 
    {
        $dbInstance = Database::getInstance();
        $this->pdo = $dbInstance->getConnection(); 
    }

    public function getCustomFields()
    {
        $sql = "SELECT * FROM " . CUSTOM_TABLE;
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getCustomField($id)
    {
        $sql = "SELECT * FROM " . CUSTOM_TABLE . " WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result;
    }

    public function getCustomFieldsByContact($id)
    {
        $sql = "SELECT * FROM " . CUSTOM_TABLE . " WHERE client_id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function addCustomField($customFieldData)
    {
        $keys = implode(",", array_keys($customFieldData));
        $questionMarks = Database::placeholders($customFieldData);
        
        $sql = "INSERT INTO " . CUSTOM_TABLE . " ($keys) VALUES ($questionMarks)";
        $stmt = $this->pdo->prepare($sql);

        $count=1;

        foreach ($customFieldData as $value) {
            $stmt->bindValue($count++, $value);
        }

        $stmt->execute();
        return $this->pdo->lastInsertId();    
    }

    public function updateCustomField($customFieldData, $id)
    {
        if(!isset($customFieldData['id']))
            unset($customFieldData['id']);                             
    
        $setValues = Database::setValues($customFieldData);             // Build query string

        $sql = "UPDATE " . CUSTOM_TABLE . " SET $setValues WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
    
        $count=1;
        foreach ($customFieldData as $value) {
            $stmt->bindValue($count++, $value);
        }
        $stmt->bindValue($count, $id);
    
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function deleteCustomField($id)
    {
        $sql = "UPDATE " . CUSTOM_TABLE . " SET Status=? WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, "Deleted");
        $stmt->bindValue(2, $id);
       
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function hardDeleteCustomField($id)
    {
        $sql = "DELETE FROM " . CUSTOM_TABLE . " WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
       
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function getStatuss() 
    {
        $sql = "SELECT status FROM " . CUSTOM_TABLE . " GROUP BY Status";
        $stmt = $this->pdo->prepare($sql);
       
        $stmt->execute();
        return $stmt->fetchAll();
    }
 
}

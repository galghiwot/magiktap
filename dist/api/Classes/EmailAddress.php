<?php
require_once 'Database.php';
const EMAILADDRESSES_TABLE = "am_email_address_info";

class EmailAddress {
    private $pdo;

    function __construct() 
    {
        $dbInstance = Database::getInstance();
        $this->pdo = $dbInstance->getConnection(); 
    }

    public function getEmailAddresses()
    {
        $sql = "SELECT * FROM " . EMAILADDRESSES_TABLE;
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getEmailAddress($id)
    {
        $sql = "SELECT * FROM " . EMAILADDRESSES_TABLE . " WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result;
    }

    public function getEmailAddressesByContact($id)
    {
        $sql = "SELECT * FROM " . EMAILADDRESSES_TABLE . " WHERE client_id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function addEmailAddress($emailAddressData)
    {
        $keys = implode(",", array_keys($emailAddressData));
        $questionMarks = Database::placeholders($emailAddressData);
        
        $sql = "INSERT INTO " . EMAILADDRESSES_TABLE . " ($keys) VALUES ($questionMarks)";
        $stmt = $this->pdo->prepare($sql);

        $count=1;

        foreach ($emailAddressData as $value) {
            $stmt->bindValue($count++, $value);
        }

        $stmt->execute();
        return $this->pdo->lastInsertId();    
    }

    public function updateEmailAddress($emailAddressData, $id)
    {
        if(!isset($emailAddressData['id']))
            unset($emailAddressData['id']);                             
    
        $setValues = Database::setValues($emailAddressData);             // Build query string

        $sql = "UPDATE " . EMAILADDRESSES_TABLE . " SET $setValues WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
    
        $count=1;
        foreach ($emailAddressData as $value) {
            $stmt->bindValue($count++, $value);
        }
        $stmt->bindValue($count, $id);
    
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function deleteEmailAddress($id)
    {
        $sql = "UPDATE " . EMAILADDRESSES_TABLE . " SET Status=? WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, "Deleted");
        $stmt->bindValue(2, $id);
       
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function hardDeleteEmailAddress($id)
    {
        $sql = "DELETE FROM " . EMAILADDRESSES_TABLE . " WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
       
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function getStatuss() 
    {
        $sql = "SELECT status FROM " . EMAILADDRESSES_TABLE . " GROUP BY Status";
        $stmt = $this->pdo->prepare($sql);
       
        $stmt->execute();
        return $stmt->fetchAll();
    }
 
}

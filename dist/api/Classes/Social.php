<?php
require_once 'Database.php';
const SOCIALS_TABLE = "am_social_info";

class Social {
    private $pdo;

    function __construct() 
    {
        $dbInstance = Database::getInstance();
        $this->pdo = $dbInstance->getConnection(); 
    }

    public function getSocials()
    {
        $sql = "SELECT * FROM " . SOCIALS_TABLE;
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getSocial($id)
    {
        $sql = "SELECT * FROM " . SOCIALS_TABLE . " WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result;
    }

    public function getSocialsByContact($id)
    {
        $sql = "SELECT * FROM " . SOCIALS_TABLE . " WHERE client_id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function addSocial($socialData)
    {
        $keys = implode(",", array_keys($socialData));
        $questionMarks = Database::placeholders($socialData);
        
        $sql = "INSERT INTO " . SOCIALS_TABLE . " ($keys) VALUES ($questionMarks)";
        $stmt = $this->pdo->prepare($sql);

        $count=1;

        foreach ($socialData as $value) {
            $stmt->bindValue($count++, $value);
        }

        $stmt->execute();
        return $this->pdo->lastInsertId();    
    }

    public function updateSocial($socialData, $id)
    {
        if(!isset($socialData['id']))
            unset($socialData['id']);                             
    
        $setValues = Database::setValues($socialData);             // Build query string

        $sql = "UPDATE " . SOCIALS_TABLE . " SET $setValues WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
    
        $count=1;
        foreach ($socialData as $value) {
            $stmt->bindValue($count++, $value);
        }
        $stmt->bindValue($count, $id);
    
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function deleteSocial($id)
    {
        $sql = "UPDATE " . SOCIALS_TABLE . " SET Status=? WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, "Deleted");
        $stmt->bindValue(2, $id);
       
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function hardDeleteSocial($id)
    {
        $sql = "DELETE FROM " . SOCIALS_TABLE . " WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
       
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function getStatuss() 
    {
        $sql = "SELECT status FROM " . SOCIALS_TABLE . " GROUP BY Status";
        $stmt = $this->pdo->prepare($sql);
       
        $stmt->execute();
        return $stmt->fetchAll();
    }
 
}

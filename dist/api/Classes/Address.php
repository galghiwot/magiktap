<?php
require_once 'Database.php';
const ADDRESSES_TABLE = "am_address_info";

class Address {
    private $pdo;

    function __construct() 
    {
        $dbInstance = Database::getInstance();
        $this->pdo = $dbInstance->getConnection(); 
    }

    public function getAddresses()
    {
        $sql = "SELECT * FROM " . ADDRESSES_TABLE;
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getAddress($id)
    {
        $sql = "SELECT * FROM " . ADDRESSES_TABLE . " WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result;
    }

    public function getAddressesByContact($id)
    {
        $sql = "SELECT * FROM " . ADDRESSES_TABLE . " WHERE client_id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function addAddress($addressData)
    {
        $keys = implode(",", array_keys($addressData));
        $questionMarks = Database::placeholders($addressData);
        
        $sql = "INSERT INTO " . ADDRESSES_TABLE . " ($keys) VALUES ($questionMarks)";
        $stmt = $this->pdo->prepare($sql);

        $count=1;

        foreach ($addressData as $value) {
            $stmt->bindValue($count++, $value);
        }

        $stmt->execute();
        return $this->pdo->lastInsertId();    
    }

    public function updateAddress($addressData, $id)
    {
        if(!isset($addressData['id']))
            unset($addressData['id']);                             
    
        $setValues = Database::setValues($addressData);             // Build query string

        $sql = "UPDATE " . ADDRESSES_TABLE . " SET $setValues WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
    
        $count=1;
        foreach ($addressData as $value) {
            $stmt->bindValue($count++, $value);
        }
        $stmt->bindValue($count, $id);
    
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function deleteAddress($id)
    {
        $sql = "UPDATE " . ADDRESSES_TABLE . " SET Status=? WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, "Deleted");
        $stmt->bindValue(2, $id);
       
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function hardDeleteAddress($id)
    {
        $sql = "DELETE FROM " . ADDRESSES_TABLE . " WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
       
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function getStatuss() 
    {
        $sql = "SELECT status FROM " . ADDRESSES_TABLE . " GROUP BY Status";
        $stmt = $this->pdo->prepare($sql);
       
        $stmt->execute();
        return $stmt->fetchAll();
    }
 
}

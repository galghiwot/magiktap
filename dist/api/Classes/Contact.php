<?php
require_once 'Database.php';
const CONTACTS_TABLE = "am_contacts";
const HASH_SALT = "Rand0m!S@lt";
const SITE_NAME = "MagikTap";

class Contact {
    private $pdo;

    function __construct() 
    {
        $dbInstance = Database::getInstance();
        $this->pdo = $dbInstance->getConnection(); 
    }

    public function getContacts()
    {
        $sql = "SELECT * FROM " . CONTACTS_TABLE;
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getContact($id)
    {
        $sql = "SELECT * FROM " . CONTACTS_TABLE . " WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result;
    }

    public function getContactsByUser($id)
    {
        $sql = "SELECT * FROM " . CONTACTS_TABLE . " WHERE Created_By = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function addContact($contactData)
    {
        $keys = implode(",", array_keys($contactData));
        $questionMarks = Database::placeholders($contactData);
        
        $sql = "INSERT INTO " . CONTACTS_TABLE . " ($keys) VALUES ($questionMarks)";
        $stmt = $this->pdo->prepare($sql);

        $count=1;
        //$contactData['pwd'] = $this->hashPassword($contactData['pwd']);
        foreach ($contactData as $value) {
            $stmt->bindValue($count++, $value);
        }

        $stmt->execute();
        return $this->pdo->lastInsertId();    
    }

    public function updateContact($contactData, $id)
    {
        /* Check for duplicate email address */
       /* $sql = "SELECT * FROM " . CONTACTS_TABLE . " WHERE email = ? OR username = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $contactData['email']);
        $stmt->bindValue(2, $contactData['username']);
        $stmt->execute();
        if($stmt->fetch())  
            return null;*/

        /** Remove id from the contactData array if present   otherwise it will be inserted in UPDATE SQL Query **/
        if(!isset($contactData['id']))
            unset($contactData['id']);                             
    
        $setValues = Database::setValues($contactData);             // Build query string

        $sql = "UPDATE " . CONTACTS_TABLE . " SET $setValues WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
    
        $count=1;
        foreach ($contactData as $value) {
            $stmt->bindValue($count++, $value);
        }
        $stmt->bindValue($count, $id);
    
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function deleteContact($id)
    {
        $sql = "UPDATE " . CONTACTS_TABLE . " SET Status=? WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, "Deleted");
        $stmt->bindValue(2, $id);
       
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function hardDeleteContact($id)
    {
        $sql = "DELETE FROM " . CONTACTS_TABLE . " WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
       
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function getStatuss() 
    {
        $sql = "SELECT status FROM " . CONTACTS_TABLE . " GROUP BY Status";
        $stmt = $this->pdo->prepare($sql);
       
        $stmt->execute();
        return $stmt->fetchAll();
    }
 
}
/*
Contact Array Strucutre
id
Name
Given_Name
Additional_Name
Family_Name
Phonetic_Name
Given_Name_Phonetic
Additional_Name_Phonetic
Family_Name_Phonetic
Name_Prefix
Name_Suffix
Initials
Nickname
Short_Name
Maiden_Name
Birthday
Gender
Location
Billing_Information
Directory_Server
Mileage
Occupation
Hobby
Sensitivity
Priority
Subject
Notes
Language
Photo
Group_Membership
E-mail_1_-_Type
E-mail_1_-_Value
IM_1_-_Type
IM_1_-_Service
IM_1_-_Value
Phone_1_-_Type
Phone_1_-_Value
Phone_2_-_Type
Phone_2_-_Value
Phone_3_-_Type
Phone_3_-_Value
Address_1_-_Type
Address_1_-_Formatted
Address_1_-_Street
Address_1_-_City
Address_1_-_PO_Box
Address_1_-_Region
Address_1_-_Postal_Code
Address_1_-_Country
Address_1_-_Extended_Address
Organization_1_-_Type
Organization_1_-_Name
Organization_1_-_Phonetic_Name
Organization_1_-_Title
Organization_1_-_Department
Organization_1_-_Symbol
Organization_1_-_Location
Organization_1_-_Job_Description
Website_1_-_Type
Website_1_-_Value
Event_1_-_Type
Event_1_-_Value
Custom_Field_1_-_Type
Custom_Field_1_-_Value
Status
*/
<?php
require_once 'Database.php';
const WEBURLS_TABLE = "am_web_url_info";

class WebUrl {
    private $pdo;

    function __construct() 
    {
        $dbInstance = Database::getInstance();
        $this->pdo = $dbInstance->getConnection(); 
    }

    public function getWebUrls()
    {
        $sql = "SELECT * FROM " . WEBURLS_TABLE;
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getWebUrl($id)
    {
        $sql = "SELECT * FROM " . WEBURLS_TABLE . " WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result = $stmt->fetch();
        return $result;
    }

    public function getWebUrlsByContact($id)
    {
        $sql = "SELECT * FROM " . WEBURLS_TABLE . " WHERE client_id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function addWebUrl($webUrlData)
    {
        $keys = implode(",", array_keys($webUrlData));
        $questionMarks = Database::placeholders($webUrlData);
        
        $sql = "INSERT INTO " . WEBURLS_TABLE . " ($keys) VALUES ($questionMarks)";
        $stmt = $this->pdo->prepare($sql);

        $count=1;

        foreach ($webUrlData as $value) {
            $stmt->bindValue($count++, $value);
        }

        $stmt->execute();
        return $this->pdo->lastInsertId();    
    }

    public function updateWebUrl($webUrlData, $id)
    {
        if(!isset($webUrlData['id']))
            unset($webUrlData['id']);                             
    
        $setValues = Database::setValues($webUrlData);             // Build query string

        $sql = "UPDATE " . WEBURLS_TABLE . " SET $setValues WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
    
        $count=1;
        foreach ($webUrlData as $value) {
            $stmt->bindValue($count++, $value);
        }
        $stmt->bindValue($count, $id);
    
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function deleteWebUrl($id)
    {
        $sql = "UPDATE " . WEBURLS_TABLE . " SET Status=? WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, "Deleted");
        $stmt->bindValue(2, $id);
       
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function hardDeleteWebUrl($id)
    {
        $sql = "DELETE FROM " . WEBURLS_TABLE . " WHERE id = ?";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(1, $id);
       
        $stmt->execute();
        return $stmt->rowCount();
    }

    public function getStatuss() 
    {
        $sql = "SELECT status FROM " . WEBURLS_TABLE . " GROUP BY Status";
        $stmt = $this->pdo->prepare($sql);
       
        $stmt->execute();
        return $stmt->fetchAll();
    }
 
}

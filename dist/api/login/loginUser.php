<?php 
    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata, true);        // Convert from object to array

    require_once '../Classes/User.php';
    require_once '../Classes/Session.php';

    $user = new User();
    $userDetails = $user->authUser($request);
    if($userDetails) {
        if($userDetails['status'] == "Active") {
            $session = Session::getInstance();
            $session->id = $userDetails['id'];
            $session->name = $userDetails['fullName'];
            $session->role = $userDetails['role'];
            $result = "User active and login successfully.";
        }
        else if($userDetails['status'] == "Inactive") {
            $result = "Sorry! Your account has not be activated. Plese contact Administrator.";
        }
        else if($userDetails['status'] == "Deleted") {
            $result = "Sorry! Your account has been deleted by Administrator.";
        }
		// headers to tell that result is JSON
		header('Content-type: application/json');
        echo json_encode(array('success'=>true, "message" => $result,
        'data'=>array("id"=>$userDetails['id'],
        "userType"=>$userDetails['user_type'],
        "firstName"=>$userDetails['fullName'])));
    }
    else {
		// headers to tell that result is JSON
		header('Content-type: application/json');
        echo json_encode(array('success'=>false,'message'=> "Sorry! Your email and password combination do not match."));
	}
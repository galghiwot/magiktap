<?php 
    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata, true);        // Convert from object to array

    require_once '../Classes/User.php';

    $user = new User();
    $userDetails = $user->resetPassword($request['emailId']);
    
    if($userDetails){
		// headers to tell that result is JSON
		header('Content-type: application/json');
        echo json_encode(array('success'=>true,'message' => "Success! Your password has been reset. Please check your email."));
    }else{
		// headers to tell that result is JSON
		header('Content-type: application/json');		
        echo json_encode(array('success'=>false,'message'=> "Sorry! Please enter your registered email address."));
	}
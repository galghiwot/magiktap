'use strict';

/**
 * Route configuration for the RDash module.
 */
angular.module('app').config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        // For unmatched routes
        $urlRouterProvider.otherwise('/');

        // Application routes
        $stateProvider
            .state('index', {
                url: '/',
                controller: 'MasterCtrl',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/home.view.html',
                        controller: 'ProfileController',
                        controllerAs: 'pc'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                },
                // templateUrl: 'templates/dashboard.html'
            })
            .state('login', {
                url: '/login',
                views: {
                    "sidebar": {
                        template: ""
                    },
                    "main": {
                        templateUrl: 'templates/login.view.html',
                        controller: 'LoginController',
                        controllerAs: 'vm'
                    },
                    "headbar": {
                        template: ""
                    }
                },
                // templateUrl: 'templates/login.view.html',

            }).state('register', {
                url: '/register',
                views: {
                    "sidebar": {
                        template: ""
                    },
                    "main": {
                        templateUrl: 'templates/register.view.html',
                        controller: 'RegisterController',
                        controllerAs: 'vm'
                    },
                    "headbar": {
                        template: ""
                    }
                }
                // templateUrl: 'templates/register.view.html',

            }).state('registerWithId', {
                url: '/code/:id/register',
                views: {
                    "sidebar": {
                        template: ""
                    },
                    "main": {
                        templateUrl: 'templates/register.view.html',
                        controller: 'RegisterController',
                        controllerAs: 'vm'
                    },
                    "headbar": {
                        template: ""
                    }
                }
                // templateUrl: 'templates/register.view.html',

            }).state('users', {
                url: '/users',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/users_index.html',
                        controller: 'UsersCtrl',
                        controllerAs: 'usersCtrl'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                }
            }).state('usersnew', {
                url: '/users/new',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/users_new.html',
                        controller: 'UsersCtrl',
                        controllerAs: 'usersCtrl'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                }
            }).state('usersedit', {
                url: '/users/:id/edit',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/users_edit.html',
                        controller: 'UsersCtrl',
                        controllerAs: 'usersCtrl',
                        method: 'edit'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                }
            }).state('usersprofile', {
                url: '/users/:id/profile',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/users_profile.html',
                        controller: 'UsersCtrl',
                        controllerAs: 'usersCtrl',
                        method: 'edit'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                }
            }).state('contactsprofile', {
                url: '/contacts/:id/profile',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/contacts_profile.html',
                        controller: 'ContactCtrl',
                        controllerAs: 'contactCtrl',
                        method: 'edit'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                }
            }).state('cardsview', {
                url: '/cards',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/cards_view.html',
                        controller: 'CardCtrl',
                        controllerAs: 'cardCtrl',
                        method: 'init'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                }
            }).state('leadgeneration', {
                url: '/leads',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/leads_view.html',
                        //controller: 'ContactCtrl',
                        //controllerAs: 'contactCtrl',
                        //method: 'edit'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                }
            }).state('contactspublic', {
                url: '/contacts/:id/public',
                views: {
                    "main": {
                        templateUrl: 'templates/contacts_public.html',
                        controller: 'ContactCtrl',
                        controllerAs: 'contactCtrl'
                    }
                }
            }).state('leadspublic', {
                url: '/leads/:id/public',
                views: {
                    "main": {
                        templateUrl: 'templates/leads_public.html',
                        controller: 'ContactCtrl',
                        controllerAs: 'contactCtrl'
                    }
                }
            }).state('contacts', {
                url: '/contacts',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/contacts_index.html',
                        controller: 'ContactCtrl',
                        controllerAs: 'contactCtrl',
                        method: 'init'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                }
            }).state('contactsnew', {
                url: '/contacts/new',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/contacts_new.html',
                        controller: 'ContactCtrl',
                        controllerAs: 'contactCtrl'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                }
            }).state('contactsedit', {
                url: '/contacts/:id/edit',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/contacts_edit.html',
                        controller: 'ContactCtrl',
                        controllerAs: 'contactCtrl',
                        method: 'edit'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                }
            }).state('organizations', {
                url: '/organizations',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/organizations_index.html',
                        controller: 'OrganizationCtrl',
                        controllerAs: 'organizationCtrl',
                        method: 'init'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                }
            }).state('organizationsnew', {
                url: '/organizations/new',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/organizations_new.html',
                        controller: 'OrganizationCtrl',
                        controllerAs: 'organizationCtrl'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                }
            }).state('organizationsedit', {
                url: '/organizations/:id/edit',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/organizations_edit.html',
                        controller: 'OrganizationCtrl',
                        controllerAs: 'organizationCtrl',
                        method: 'edit'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                }
            }).state('clientsprofile', {
                url: '/clients/:id/profile',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/clients_profile.html',
                        controller: 'ClientCtrl',
                        controllerAs: 'clientCtrl',
                        method: 'edit'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                }
            }).state('clientsedit', {
                url: '/clients/:id/edit',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/clients_edit.html',
                        controller: 'ClientCtrl',
                        controllerAs: 'clientCtrl',
                        method: 'edit'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                }
            }).state('clientsnew', {
                url: '/clients/:id/new',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/clients_new.html',
                        controller: 'ClientCtrl',
                        controllerAs: 'clientCtrl',
                        method: 'new'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                }
            }).state('clientspublic', {
                url: '/clients/:id/public',
                views: {
                    "main": {
                        templateUrl: 'templates/clients_public.html',
                        controller: 'ClientCtrl',
                        controllerAs: 'clientCtrl'
                    }
                }
            }).state('clientsbyorg', {
                url: '/clients/:id/view',
                views: {
                    "sidebar": {
                        templateUrl: "templates/sidebar.html"
                    },
                    "main": {
                        templateUrl: 'templates/clients_index.html',
                        controller: 'ClientCtrl',
                        controllerAs: 'clientCtrl',
                        method: 'init'
                    },
                    "headbar": {
                        templateUrl: "templates/headbar.html",
                        controller: 'MasterCtrl',
                        controllerAs: 'mc'
                    }
                }
            });
    }]).run(['$rootScope', '$location', '$cookieStore', '$http', function ($rootScope, $location, $cookieStore, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }
        $rootScope.globals.serviceUrl = "http://127.0.0.1/apps.magiktap/dist";
        console.log("Service URL: " + $rootScope.globals.serviceUrl);
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            console.log("Turn the tables and start gathering leads: " + $location.path());
            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/login', '/register$']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if ($location.path().match('/public$') || $location.path().match('/register')) {
                console.log("Do Nothing!");
            } else if (restrictedPage && !loggedIn) {
                $location.path('/login');
            }
        });
    }]).filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';
            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;
            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace !== -1) {
                    //Also remove . and , so its gives a cleaner result.
                    if (value.charAt(lastspace - 1) === '.' || value.charAt(lastspace - 1) === ',') {
                        lastspace = lastspace - 1;
                    }
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    });

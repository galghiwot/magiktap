(function () {
    'use strict';

    angular
        .module('app')
        .factory('ContactService', ContactService);

    ContactService.$inject = ['$http', '$cookieStore', '$rootScope', '$timeout', 'CONFIG'];
    function ContactService($http, $cookieStore, $rootScope, $timeout, CONFIG) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.GetByUserId = GetByUserId;


        
        var URL = CONFIG.API_URL+"/api/admin/contact";
        return service;

        function GetAll() {
            return $http.get(URL +'/getContacts.php').then(handleSuccess, handleError('Error getting all contacts'));
        }

        function GetById(id) {
            return $http.post(URL+'/getContact.php', id).then(handleSuccess, handleError('Error getting contact by id'));
        }
        
        function GetByUserId(id) {
            return $http.post(URL + '/getContactsByUser.php', id).then(handleSuccess, handleError('Error getting contact by id'));
        }

        function Create(contact) { 
            return $http.post(URL + '/addContact.php', contact).then(handleSuccess, handleError('Error creating contact'));
        }

        function Update(contact) {
            return $http.post(URL +'/updateContact.php', contact).then(handleSuccess, handleError('Error updating contact'));
        }

        function Delete(id) {
            return $http.post(URL +'/deleteContact.php', id).then(handleSuccess, handleError('Error deleting contact'));
        }

        // private functions
        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();

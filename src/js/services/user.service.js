(function () {
    'use strict';

    angular
        .module('app')
        .factory('UserService', UserService);

    UserService.$inject = ['$http', '$cookieStore', '$rootScope', '$timeout', 'CONFIG'];
    function UserService($http, $cookieStore, $rootScope, $timeout, CONFIG) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        
        var URL = CONFIG.API_URL+"/api/admin/user";
        return service;

        function GetAll() {
            return $http.get(URL +'/getUsers.php').then(handleSuccess, handleError('Error getting all users'));
        }

        function GetById(id) {
            return $http.post(URL+'/getUser.php', id).then(handleSuccess, handleError('Error getting user by id'));
        }

        function Create(user) { 
            return $http.post(URL + '/addUser.php', user).then(handleSuccess, handleError('Error creating user'));
        }

        function Update(user) {
            return $http.post(URL +'/updateUser.php', user).then(handleSuccess, handleError('Error updating user'));
        }

        function Delete(id) {
            return $http.post(URL +'/deleteUser.php', id).then(handleSuccess, handleError('Error deleting user'));
        }

        // private functions

        function handleSuccess(res) {
           return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();

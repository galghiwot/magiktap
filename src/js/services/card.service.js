(function () {
    'use strict';

    angular
        .module('app')
        .factory('CardService', CardService);

    CardService.$inject = ['$http', '$cookieStore', '$rootScope', '$timeout', 'CONFIG'];
    function CardService($http, $cookieStore, $rootScope, $timeout, CONFIG) {
        var service = {};
        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        var URL = CONFIG.API_URL+"/api/admin/card";
        return service;

        function GetAll() {
            return $http.get(URL +'/getCards.php').then(handleSuccess, handleError('Error getting all cards'));
        }

        function GetById(id) {
            return $http.post(URL+'/getCard.php', id).then(handleSuccess, handleError('Error getting card by id'));
        }

        function Create(card) { 
            return $http.post(URL + '/addCard.php', card).then(handleSuccess, handleError('Error creating card'));
        }

        function Update(card) {
            return $http.post(URL +'/updateCard.php', card).then(handleSuccess, handleError('Error updating card'));
        }

        function Delete(id) {
            return $http.post(URL +'/deleteCard.php', id).then(handleSuccess, handleError('Error deleting card'));
        }

        // private functions
        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();

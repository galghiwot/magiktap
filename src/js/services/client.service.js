(function () {
    'use strict';

    angular
        .module('app')
        .factory('ClientService', ClientService);

    ClientService.$inject = ['$http', '$cookieStore', '$rootScope', '$timeout', 'CONFIG'];
    function ClientService($http, $cookieStore, $rootScope, $timeout, CONFIG) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.GetByOrganizationId = GetByOrganizationId;
        service.GetByUserId = GetByUserId;


        
        var URL = CONFIG.API_URL+"/api/admin/client";
        return service;

        function GetAll() {
            return $http.get(URL +'/getClients.php').then(handleSuccess, handleError('Error getting all clients'));
        }

        function GetById(id) {
            return $http.post(URL+'/getClient.php', id).then(handleSuccess, handleError('Error getting client by id'));
        }
        
        function GetByUserId(id) {
            return $http.post(URL + '/getClientsByUser.php', id).then(handleSuccess, handleError('Error getting client by id'));
        }
        
        function GetByOrganizationId(id) {
            return $http.post(URL + '/getClientsByOrganization.php', id).then(handleSuccess, handleError('Error getting client by id'));
        }

        function Create(client) { 
            return $http.post(URL + '/addClient.php', client).then(handleSuccess, handleError('Error creating client'));
        }

        function Update(client) {
            return $http.post(URL +'/updateClient.php', client).then(handleSuccess, handleError('Error updating client'));
        }

        function Delete(id) {
            return $http.post(URL +'/deleteClient.php', id).then(handleSuccess, handleError('Error deleting client'));
        }

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();

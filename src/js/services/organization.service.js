(function () {
    'use strict';

    angular
        .module('app')
        .factory('OrganizationService', OrganizationService);

    OrganizationService.$inject = ['$http', '$cookieStore', '$rootScope', '$timeout', 'CONFIG'];
    function OrganizationService($http, $cookieStore, $rootScope, $timeout, CONFIG) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.GetByUserId = GetByUserId;


        
        var URL = CONFIG.API_URL+"/api/admin/organization";
        return service;

        function GetAll() {
            return $http.get(URL +'/getOrganizations.php').then(handleSuccess, handleError('Error getting all organizations'));
        }

        function GetById(id) {
            return $http.post(URL+'/getOrganization.php', id).then(handleSuccess, handleError('Error getting organization by id'));
        }
        
        function GetByUserId(id) {
            return $http.post(URL + '/getOrganizationsByUser.php', id).then(handleSuccess, handleError('Error getting organization by id'));
        }

        function Create(organization) { 
            return $http.post(URL + '/addOrganization.php', organization).then(handleSuccess, handleError('Error creating organization'));
        }

        function Update(organization) {
            return $http.post(URL +'/updateOrganization.php', organization).then(handleSuccess, handleError('Error updating organization'));
        }

        function Delete(id) {
            return $http.post(URL +'/deleteOrganization.php', id).then(handleSuccess, handleError('Error deleting organization'));
        }

        // private functions
        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();

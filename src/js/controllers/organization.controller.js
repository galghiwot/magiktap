/* globals confirm */
(function () {
    'use strict';
    //AlertService
    function OrganizationCtrl($log, $q, $scope, $rootScope, $location, $stateParams, $state, $filter, OrganizationService) {

        // this.AlertService = AlertService;

        // this.alerts = $rootScope.alerts;

        /**
         * Initial value of form
         *
         * @type {Array}
         */
        var vm = this;
        vm.social = {};
        vm.organization = [];
        vm.listOrganization = [];
        
        this.itemsByPage = 15;
        vm.serviceUrl = $rootScope.globals.serviceUrl;
        
        /**
         * Reset the form values
         */

        this.reset = function () {
            this.organization = [
                {
                    id: "1",
                    parent_id: "2",
                    type: "0",
                    name: "Signage and Cards",
                    address_line1: "123 Main St",
                    addressLine2: null,
                    city: "Charlotte",
                    state: "NC",
                    zip_code: "Mecklenburg",
                    phone_number: "4055099042",
                    email: "admin@signage.com",
                    time_zone: "ETC",
                    friendly_url: "signage",
                    logo_url: "signage.jpg",
                    website: null,
                    about_org: "0",
                    super_user_id: "1",
                    status: ""
                }
            ];
        };

        /**
         * Add a listOrganization in vm.listOrganization
         */
        this.create = function (organization) {
           OrganizationService.Create(organization);
            //   this.AlertService.add('success', 'Provider "' + organization.name + '" created with success!', 5000);
        };

        /**
         * Editing a individual organization
         * @return {[type]}     [description]
         */
        this.edit = function () {
            var id = $stateParams.id;
            //$log.info("The id to edit is: " + id);           
            var deferredCat = $q.defer();
            var getOrganizationsPromise = deferredCat.promise;
            getOrganizationsPromise = OrganizationService.GetById(id);
            getOrganizationsPromise
                .then(function (res) {
                    vm.organization = res.data;
                    vm.organization.status="Active";
                    $log.info("Organization Data: " + JSON.stringify(vm.organization));
                     },
                    function (error) {
                        //$log.info(error);
                    },
                    function (progress) {
                        //$log.info(progress);
                    },
                    function () {
                        //$log.info("Nothing...");
                    });
            window.scrollTo(0, 0);
        };
        /**
         * Update item
         * @param  {Object} item [description]
         * @return {[type]}      [description]
         */
        this.update = function (item) {
            $log.info("The id to update is: " + JSON.stringify(item));
            vm.organization.status = "Active";
            vm.Organization = OrganizationService.Update(item);

            //   this.AlertService.add('success', 'Provider "' + item.name + '" updated with success!', 5000);
        };

        /**
         * Add/edit method abstration
         * @param  {Object} item [description]
         * @return {[type]}      [description]
         */
        this.save = function (item) {
            if (typeof item.id !== 'undefined') {
                this.update(item);
            } else {
                this.create(item);
            }
            this.reset();
            $location.path('/organizations');
        };

        /**
         * [delete description]
         * @param  {Integer} index        [description]
         * @param  {Boolean} confirmation [description]
         * @return {Boolean}              [description]
         */
        this.delete = function (index, confirmation) {
            confirmation = (typeof confirmation !== 'undefined') ? confirmation : true;
            if (confirmDelete(confirmation)) {
                var message,
                    item = OrganizationService.Delete(index);
                if (!!item) {
                    // message = 'Provider "' + item.name + '" was removed of your organization\'s list';
                    // this.AlertService.add('success', message, 5000);
                    vm.listOrganization = OrganizationService.GetAll();
                    return true;
                }
                //  this.AlertService.add('error', 'Houston, we have a problem. This operation cannot be executed correctly.', 5000);
                return false;
            }
        };

        /**
         * Method for access "window.confirm" function
         * @param  {Boolean} confirmation [description]
         * @return {Boolean}              [description]
         */
        var confirmDelete = function (confirmation) {
            return confirmation ? confirm('This action is irreversible. Do you want to delete this organization?') : true;
        };

        /**
         * Method for class initialization
         * @return {[type]} [description]
         */
        this.init = function () {
            var deferredCat = $q.defer();
            var getOrganizationsPromise = deferredCat.promise;
            getOrganizationsPromise = OrganizationService.GetAll();
            getOrganizationsPromise
                .then(function (res) {
                    if (res) {
                        vm.listOrganization = res.data;
                        //$log.info("Organizations Loaded" + vm.listOrganization.length);
                    }
                },
                    function (error) {
                        //$log.info(error);
                    },
                    function (progress) {
                        //$log.info(progress);
                    },
                    function () {
                        //$log.info("Nothing...");
                    });
        };
        //this.init();
        var tabClasses;

        function initTabs() {
            tabClasses = ["", ""];
        }

        this.getTabClass = function (tabNum) {
            return tabClasses[tabNum];
        };

        this.getTabPaneClass = function (tabNum) {
            return "tab-pane " + tabClasses[tabNum];
        }

        this.setActiveTab = function (tabNum) {
            initTabs();
            tabClasses[tabNum] = "active";
        };

        //Initialize 
        initTabs();
        this.setActiveTab(1);
        this.edit();
    }

    angular.module('app')
        .controller('OrganizationCtrl', OrganizationCtrl);
    //'AlertService',
    OrganizationCtrl.$inject = ['$log', '$q', '$scope', '$rootScope', '$location', '$stateParams', '$state', '$filter', 'OrganizationService'];

}());

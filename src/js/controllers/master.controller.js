/**
 * Master Controller
 */
//$log.info('MasterController');
angular.module('app')
    .controller('MasterCtrl', ['$log', '$rootScope', '$scope', '$state', '$cookieStore', MasterCtrl]);

function MasterCtrl($log, $rootScope, $scope, $state, $cookieStore) {
    var mc = this;
    //$log.info('Master Controller Inside');
    //$state.go("/");
    /**
     * Sidebar Toggle & Cookie Control
     */
    var mobileView = 992;
    $scope.labels = ['2015-11-01', '2015-11-02', '2015-11-03', '2015-11-04', '2015-11-05', '2015-11-06', '2015-11-07'];
    $scope.series = ['Provider 1', 'Provider 2', 'Provider 3', 'Provider 4'];
    
    mc.currentUser = $rootScope.globals.currentUser;
    $log.info("Current User: " + JSON.stringify($rootScope.globals.currentUser));
 
    $scope.data = [
        [Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100)],
        [Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100)],
        [Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100)],
        [Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100), Math.floor(Math.random() * 100)]
    ];
    $scope.options = {
        yAxisLabel: "Record Count",
    }

    var paymentProviderList = ['M-PESA', 'Airtel Kenya', 'Telkom Kenya'],
        shortCodeProviderList = ['Safaricom', 'Airtel', 'National Telecom'];

    var scope = [];

    function randomDate(start, end) {
        return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    }



    function createRandomItem() {

        var
            providerName = "Provider" + Math.floor(Math.random() * 4) + 1,
            paymentProvider = paymentProviderList[Math.floor(Math.random() * 2)],
            FromAddress = "+" + (25490922000 + Math.floor(Math.random() * 100));

        return {
            provider: paymentProvider,
            patientrecordId: Math.floor(Math.random() * 4000),
            dateTime: randomDate(new Date(2015, 0, 1), new Date()),
            amount: Math.floor(Math.random() * 4000),
            fee: 2,
            fromAddress: '0xbb512c0f0e6287dd1e4d8de39205f73f5d8b76a5',
            toAddress: '0xbb512c0f0e6287dd1e4d8de39205f73f5d8b76a' + 2,
            PathToFile: "http://lorempixel.com/400/200/abstract/1/Dummy-Text/",
            Catagory: "1",
            FileHash: "bd686896fe90822e7dd970967fc88fe9e9a671b7b80eb3b35485d25d8ee04625"
        };


    }

    for (var j = 0; j < 200; j++) {
        //  //$log.info(createRandomItem());
        scope.push(createRandomItem());
    }
    $scope.displayed = scope;


    $scope.getWidth = function () {
        return window.innerWidth;
    };

    $scope.$watch($scope.getWidth, function (newValue, oldValue) {
        if (newValue >= mobileView) {
            if (angular.isDefined($cookieStore.get('toggle'))) {
                $scope.toggle = !$cookieStore.get('toggle') ? false : true;
            } else {
                $scope.toggle = true;
            }
        } else {
            $scope.toggle = false;
        }

    });

    $scope.toggleSidebar = function () {
        $scope.toggle = !$scope.toggle;
        $cookieStore.put('toggle', $scope.toggle);
    };


    $scope.isIndividual = function(){
        return 1 == $rootScope.globals.currentUser.userType;
    };

    $scope.isOrganization = function () {
        return 2 == $rootScope.globals.currentUser.userType;
    };

    $scope.isReseller = function () {
        return 3 == $rootScope.globals.currentUser.userType;
    };

    window.onresize = function () {
        $scope.$apply();
    };
}

Chart.types.Bar.extend({
    name: "BarAlt",
    initialize: function (data) {
        if (this.options.yAxisLabel) this.options.scaleLabel = '         ' + this.options.scaleLabel;

        Chart.types.Bar.prototype.initialize.apply(this, arguments);

        if (this.options.yAxisLabel) this.scale.yAxisLabel = this.options.yAxisLabel;
    },
    draw: function () {
        Chart.types.Bar.prototype.draw.apply(this, arguments);

        if (this.scale.yAxisLabel) {
            var ctx = this.chart.ctx;
            ctx.save();
            // text alignment and color
            ctx.textAlign = "center";
            ctx.textBaseline = "bottom";
            ctx.fillStyle = this.options.scaleFontColor;
            // position
            var x = this.scale.xScalePaddingLeft * 0.2;
            var y = this.chart.height / 2;
            // change origin
            ctx.translate(x, y)
            // rotate text
            ctx.rotate(-90 * Math.PI / 180);
            ctx.fillText(this.scale.yAxisLabel, 0, 0);
            ctx.restore();
        }
    }
});

angular.module('chart.js')
    .directive('chartBarAlt', ['ChartJsFactory', function (ChartJsFactory) {
        return new ChartJsFactory('BarAlt');
    }]);

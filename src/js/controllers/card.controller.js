/* globals confirm */
(function () {
    'use strict';
    //AlertService
    function CardCtrl($log, $q, $scope, $rootScope, $location, $stateParams, $state, $filter, CardService) {

        // this.AlertService = AlertService;

        // this.alerts = $rootScope.alerts;

        /**
         * Initial value of form
         *
         * @type {Array}
         */
        var vm = this;
        vm.card = [];
        vm.listCard = [];
        this.itemsByPage = 15;
        vm.serviceUrl = $rootScope.globals.serviceUrl;
        /**
         * Reset the form values
         */

        this.reset = function () {
            this.card = [
                {
                    id: '',
                    Contatact_id: '',
                    Status: ''
                }
            ];
        };

        /**
         * Add a listCard in vm.listCard
         */
        this.create = function (card) {
            CardService.Create(card);
            //   this.AlertService.add('success', 'Provider "' + card.name + '" created with success!', 5000);
        };

        /**
         * Editing a individual card
         * @return {[type]}     [description]
         */
        this.edit = function () {
            var id = $stateParams.id;
            //$log.info("The id to edit is: " + id);
            var deferredCat = $q.defer();
            var getCardsPromise = deferredCat.promise;
            getCardsPromise = CardService.GetById(id);
            getCardsPromise
                .then(function (res) {
                    vm.card = res.data;
                    //$log.info("Card Data: " + JSON.stringify(vm.card));
                },
                    function (error) {
                        //$log.info(error);
                    },
                    function (progress) {
                        //$log.info(progress);
                    },
                    function () {
                        //$log.info("Nothing...");
                    });
            window.scrollTo(0, 0);
        };
        /**
         * Update item
         * @param  {Object} item [description]
         * @return {[type]}      [description]
         */
        this.update = function (item) {
            //$log.info("The id to update is: " + JSON.stringify(item));

            vm.Card = CardService.Update(item);

            //   this.AlertService.add('success', 'Provider "' + item.name + '" updated with success!', 5000);
        };

        /**
         * Add/edit method abstration
         * @param  {Object} item [description]
         * @return {[type]}      [description]
         */
        this.save = function (item) {
            if (typeof item.id !== 'undefined') {
                this.update(item);
            } else {
                this.create(item);
            }
            this.reset();
            $location.path('/cards');
        };

        /**
         * [delete description]
         * @param  {Integer} index        [description]
         * @param  {Boolean} confirmation [description]
         * @return {Boolean}              [description]
         */
        this.delete = function (index, confirmation) {
            confirmation = (typeof confirmation !== 'undefined') ? confirmation : true;
            if (confirmDelete(confirmation)) {
                var message,
                    item = CardService.Delete(index);
                if (!!item) {
                    // message = 'Provider "' + item.name + '" was removed of your card\'s list';
                    // this.AlertService.add('success', message, 5000);
                    vm.listCard = CardService.GetAll();
                    return true;
                }
                //  this.AlertService.add('error', 'Houston, we have a problem. This operation cannot be executed correctly.', 5000);
                return false;
            }
        };

        /**
         * Method for access "window.confirm" function
         * @param  {Boolean} confirmation [description]
         * @return {Boolean}              [description]
         */
        var confirmDelete = function (confirmation) {
            return confirmation ? confirm('This action is irreversible. Do you want to delete this card?') : true;
        };

        /**
         * Method for class initialization
         * @return {[type]} [description]
         */
        this.init = function () {
            var deferredCat = $q.defer();
            var getCardsPromise = deferredCat.promise;
            getCardsPromise = CardService.GetAll();
            getCardsPromise
                .then(function (res) {
                    if (res) {
                        vm.listCard = res.data;
                        //$log.info("Cards Loaded" + vm.listCard.length);
                    }
                },
                    function (error) {
                        //$log.info(error);
                    },
                    function (progress) {
                        //$log.info(progress);
                    },
                    function () {
                        //$log.info("Nothing...");
                    });
        };
        //this.init();
        var tabClasses;

        function initTabs() {
            tabClasses = ["", ""];
        }

        this.getTabClass = function (tabNum) {
            return tabClasses[tabNum];
        };

        this.getTabPaneClass = function (tabNum) {
            return "tab-pane " + tabClasses[tabNum];
        }

        this.setActiveTab = function (tabNum) {
            initTabs();
            tabClasses[tabNum] = "active";
        };

        //Initialize 
        initTabs();
        this.setActiveTab(1);
    }

    angular.module('app')
        .controller('CardCtrl', CardCtrl);
    //'AlertService',
    CardCtrl.$inject = ['$log', '$q', '$scope', '$rootScope', '$location', '$stateParams', '$state', '$filter', 'CardService'];

}());

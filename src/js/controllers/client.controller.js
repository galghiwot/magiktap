/* globals confirm */
(function () {
    'use strict';
    //AlertService
    function ClientCtrl($log, $q, $scope, $rootScope, $location, $stateParams, $state, $filter, ClientService) {

        // this.AlertService = AlertService;

        // this.alerts = $rootScope.alerts;

        /**
         * Initial value of form
         *
         * @type {Array}
         */
        var vm = this;
        vm.social = {};
        vm.client = [];
        vm.email = {
            id: "",
            label: "",
            email_address: "",
            client_id: "",
            Status: "Active"
        };

        vm.phone = {
            id: "",
            label: "",
            number: "",
            client_id: "",
            Status: "Active"
        };

        vm.address = {
            id: "",
            label: "",
            address_line1: "",
            address_line2: "",
            city: "",
            state: "",
            zipcode: "",
            county: "",
            country: "US",
            client_id: "",
            Status: "Active"
        };
        vm.webUrl = {
            id: "",
            label: "",
            url: "",
            client_id: "",
            Status: "Active"
        };

        vm.customField = {
            id: "",
            label: "",
            value: "",
            client_id: "",
            Status: "Active"
        };
        vm.listClient = [];
        vm.organization_id = 1;
        vm.itemsByPage = 15;
        vm.serviceUrl = $rootScope.globals.serviceUrl;
        /**
         * Reset the form values
         */
        this.reset = function () {
            this.client = {};
            this.client.names={};
            this.client.emailAddresses =[];
            this.client.addresses=[];
            this.client.phones=[];
            this.client.webUrls=[];
            this.client.customFields=[];
            this.client.socials=[];

             /* [
                {
                    id: '',
                    Name: '',
                    Given_Name: '',
                    Additional_Name: '',
                    Family_Name: '',
                    Phonetic_Name: '',
                    Given_Name_Phonetic: '',
                    Additional_Name_Phonetic: '',
                    Family_Name_Phonetic: '',
                    Name_Prefix: '',
                    Name_Suffix: '',
                    Initials: '',
                    Nickname: '',
                    Short_Name: '',
                    Maiden_Name: '',
                    Birthday: '',
                    Gender: '',
                    Location: '',
                    Billing_Information: '',
                    Directory_Server: '',
                    Mileage: '',
                    Occupation: '',
                    Hobby: '',
                    Sensitivity: '',
                    Priority: '',
                    Subject: '',
                    Notes: '',
                    Language: '',
                    Photo: '',
                    Group_Membership: '',
                    Email_1_Type: '',
                    Email_1_Value: '',
                    IM_1_Type: '',
                    IM_1_Service: '',
                    IM_1_Value: '',
                    Phone_1_Type: '',
                    Phone_1_Value: '',
                    Phone_2_Type: '',
                    Phone_2_Value: '',
                    Phone_3_Type: '',
                    Phone_3_Value: '',
                    Address_1_Type: '',
                    Address_1_Formatted: '',
                    Address_1_Street: '',
                    Address_1_City: '',
                    Address_1_PO_Box: '',
                    Address_1_Region: '',
                    Address_1_Postal_Code: '',
                    Address_1_Country: '',
                    Address_1_Extended_Address: '',
                    Organization_1_Type: '',
                    Organization_1_Name: '',
                    Organization_1_Phonetic_Name: '',
                    Organization_1_Title: '',
                    Organization_1_Department: '',
                    Organization_1_Symbol: '',
                    Organization_1_Location: '',
                    Organization_1_Job_Description: '',
                    Website_1_Type: '',
                    Website_1_Value: '',
                    Event_1_Type: '',
                    Event_1_Value: '',
                    Custom_Field_1_Type: '',
                    Custom_Field_1_Value: '',
                    Status: '',
                    Created_By: '',
                    Modified_By: '',
                    Created_At: '',
                    Modified_At: ''
                }
            ];*/
        };

        /**
         * Add a listClient in vm.listClient
         */
        this.create = function (client) {
            ClientService.Create(client);
            //   this.AlertService.add('success', 'Provider "' + client.name + '" created with success!', 5000);
        };

        /**
         * Add a listClient in vm.listClient
         */
        this.new = function () {
            var id = $stateParams.id;
            this.reset();
            vm.client.names.organization_id =id;
            //   this.AlertService.add('success', 'Provider "' + client.name + '" created with success!', 5000);
        };

        /**
         * Editing a individual client
         * @return {[type]}     [description]
         */
        this.edit = function () {
            var id = $stateParams.id;            
            //$log.info("The id to edit is: " + id);           
            var deferredCat = $q.defer();
            var getClientsPromise = deferredCat.promise;
            getClientsPromise = ClientService.GetById(id);
            getClientsPromise
                .then(function (res) {
                    vm.client = res.data;
                },
                function (error) {
                    //$log.info(error);
                },
                function (progress) {
                    //$log.info(progress);
                },
                function () {
                    //$log.info("Nothing...");
                });
            window.scrollTo(0, 0);
        };
        /**
         * Update item
         * @param  {Object} item [description]
         * @return {[type]}      [description]
         */
        this.update = function (item) {
            //$log.info("The id to update is: " + JSON.stringify(item));
            $log.info("Updating......in Update method");
            vm.Client = ClientService.Update(item);

            //   this.AlertService.add('success', 'Provider "' + item.name + '" updated with success!', 5000);
        };

        /**
         * Add/edit method abstration
         * @param  {Object} item [description]
         * @return {[type]}      [description]
         */
        this.save = function (item) {
            $log.info("Client Data: " + JSON.stringify(item));
            if (typeof item.names.id !== '') {
                $log.info("Updating......");
                this.update(item);
            } else {
                $log.info("Creating......");
                this.update(item);
            }
            this.reset();
            $location.path('/clients');
        };

        /**
         * [delete description]
         * @param  {Integer} index        [description]
         * @param  {Boolean} confirmation [description]
         * @return {Boolean}              [description]
         */
        this.delete = function (index, confirmation) {
            confirmation = (typeof confirmation !== 'undefined') ? confirmation : true;
            if (confirmDelete(confirmation)) {
                var message,
                    item = ClientService.Delete(index);
                if (!!item) {
                    // message = 'Provider "' + item.name + '" was removed of your client\'s list';
                    // this.AlertService.add('success', message, 5000);
                    vm.listClient = ClientService.GetAll();
                    return true;
                }
                //  this.AlertService.add('error', 'Houston, we have a problem. This operation cannot be executed correctly.', 5000);
                return false;
            }
        };

        /**
         * Method for access "window.confirm" function
         * @param  {Boolean} confirmation [description]
         * @return {Boolean}              [description]
         */
        var confirmDelete = function (confirmation) {
            return confirmation ? confirm('This action is irreversible. Do you want to delete this client?') : true;
        };

        /**
         * Method for class initialization
         * @return {[type]} [description]
         */
        this.init = function () {
            var id = $stateParams.id;
            vm.organization_id = $stateParams.id;
            var deferredCat = $q.defer();
            var getClientsPromise = deferredCat.promise;
            getClientsPromise = ClientService.GetByOrganizationId(id);
            getClientsPromise
                .then(function (res) {
                    if (res) {
                        vm.listClient = res.data;
                        //$log.info("Clients Loaded" + vm.listClient.length);
                    }
                },
                    function (error) {
                        //$log.info(error);
                    },
                    function (progress) {
                        //$log.info(progress);
                    },
                    function () {
                        //$log.info("Nothing...");
                    });
        };
        //this.init();
        var tabClasses;

        function initTabs() {
            tabClasses = ["", ""];
        }

        this.getTabClass = function (tabNum) {
            return tabClasses[tabNum];
        };

        this.getTabPaneClass = function (tabNum) {
            return "tab-pane " + tabClasses[tabNum];
        }

        this.setActiveTab = function (tabNum) {
            initTabs();
            tabClasses[tabNum] = "active";
        };

        this.addElement = function (type, action) {
            if (type == 1) {
                if (action == 0) {
                    this.isAddEmail = true;
                } else if (action == 2) {
                    this.isAddEmail = false;
                } else {
                    var newEmail = {};
                    newEmail.label = vm.email.label;
                    newEmail.client_id = vm.client.names.id;
                    newEmail.email_address = vm.email.email_address;
                    newEmail.status = "Active";
                    vm.client.emailAddresses.push(newEmail);
                    this.isAddEmail = false;
                }
                vm.email = {
                    id: "",
                    label: "",
                    email_address: "",
                    client_id: "",
                    status: "Active"
                };
            } else if (type == 2) {
                if (action == 0) {
                    this.isAddAddress = true;
                } else if (action == 2) {
                    this.isAddAddress = false;
                } else {
                    var newAddress = {};
                    newAddress.label = vm.address.label;
                    newAddress.client_id = vm.client.names.id;
                    newAddress.address_line1 = vm.address.address_line1;
                    newAddress.address_line2 = vm.address.address_line2;
                    newAddress.city = vm.address.city;
                    newAddress.state = vm.address.state;
                    newAddress.zipcode = vm.address.zipcode;
                    newAddress.county = vm.address.county;
                    newAddress.country = vm.address.country;
                    newAddress.status = "Active";
                    vm.client.addresses.push(newAddress);
                    this.isAddAddress = false;
                }
                vm.address = {
                    id: "",
                    label: "Home",
                    address_line1: "10320 Wheatside Drive",
                    address_line2: "Apt Z",
                    city: "Charlotte",
                    state: "NC",
                    zipcode: "28262",
                    county: "Mecklenburg",
                    country: "US",
                    client_id: "1",
                    status: ""
                };
            } else if (type == 3) {
                if (action == 0) {
                    this.isAddPhone = true;
                } else if (action == 2) {
                    this.isAddPhone = false;
                } else {
                    var newPhone = {};
                    newPhone.label = vm.phone.label;
                    newPhone.client_id = vm.client.names.id;
                    newPhone.number = vm.phone.number;
                    newPhone.status = "Active";
                    vm.client.phones.push(newPhone);
                    this.isAddPhone = false;
                }
                vm.phone = {
                    id: "",
                    label: "",
                    number: "",
                    client_id: "",
                    status: "Active"
                };
            } else if (type == 4) {
                if (action == 0) {
                    this.isAddWebUrl = true;
                } else if (action == 2) {
                    this.isAddWebUrl = false;
                } else {
                    var newWebUrl = {};
                    newWebUrl.label = vm.webUrl.label;
                    newWebUrl.client_id = vm.client.names.id;
                    newWebUrl.url = vm.webUrl.url;
                    newWebUrl.status = "Active";
                    vm.client.webUrls.push(newWebUrl);
                    this.isAddWebUrl = false;
                }
                vm.webUrl = {
                    id: "",
                    label: "",
                    url: "",
                    client_id: "",
                    status: "Active"
                };
            }
            else if (type == 5) {
                if (action == 0) {
                    this.isAddCustomField = true;
                } else if (action == 2) {
                    this.isAddCustomField = false;
                } else {
                    var newCustomField = {};
                    newCustomField.label = vm.customField.label;
                    newCustomField.client_id = vm.client.names.id;
                    newCustomField.value = vm.customField.value;
                    newCustomField.status = "Active";
                    vm.client.customFields.push(newCustomField);
                    this.isAddCustomField = false;
                }
                vm.customField = {
                    id: "",
                    label: "",
                    value: "",
                    client_id: "",
                    status: "Active"
                };
            }
        }

        this.removeElement = function (type, item) {
            if(type == 1){
                var index = vm.client.emailAddresses.indexOf(item);
                //vm.client.emailAddresses.splice(index, 1);
                vm.client.emailAddresses[index].status = "InActive";  
            }else if(type == 2){
                var index = vm.client.addresses.indexOf(item);
                //vm.client.addresses.splice(index, 1);
                vm.client.addresses[index].status = "InActive";
            } else if (type == 3) {
                var index = vm.client.phones.indexOf(item);
                //vm.client.phones.splice(index, 1);
                vm.client.phones[index].status = "InActive";
            } else if (type == 4) {
                var index = vm.client.webUrls.indexOf(item);
                //vm.client.webUrls.splice(index, 1);
                vm.client.webUrls[index].status = "InActive";
            } else if (type == 5) {
                var index = vm.client.customFields.indexOf(item);
                //vm.client.customFields.splice(index, 1);
                vm.client.customFields[index].status = "InActive";
            }
            
        }

        this.isAddEmail = false;
        this.isAddPhone = false;
        this.isAddAddress = false;
        this.isAddWebUrl = false;
        this.isAddCustomField = false;
        //Initialize 
        initTabs();
        this.setActiveTab(1);
    }

    angular.module('app')
        .controller('ClientCtrl', ClientCtrl);
    //'AlertService',
    ClientCtrl.$inject = ['$log', '$q', '$scope', '$rootScope', '$location', '$stateParams', '$state', '$filter', 'ClientService'];

}());

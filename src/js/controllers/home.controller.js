(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$log', '$q', 'UserService', '$rootScope', '$scope', '$location', '$stateParams', '$state', '$filter', 'ContactService' ];

    function HomeController($log, $q, UserService, $rootScope, $scope, $location, $stateParams, $state, $filter, ContactService) {
        var vm = this;
        vm.date = new Date();
        vm.user = null;
        vm.allUsers = [];
        vm.deleteUser = deleteUser;
        vm.contact =[];
        initController();
           

        function initController() {
            loadCurrentUser();
            initTabs();
            //loadAllUsers();
        }

        function loadCurrentUser() {
            UserService.GetById($rootScope.globals.currentUser.id)
                .then(function (user) {
                    vm.user = user.data;
                    //$log.info("The user profile : " + JSON.stringify(vm.user));
                    // vm.listUsers = UserService.GetAll();
                });
        }

       

        function loadAllUsers() {
            UserService.GetAll()
                .then(function (users) {
                    vm.allUsers = users;
                });
        }

        function deleteUser(id) {
            UserService.Delete(id)
                .then(function () {
                    loadAllUsers();
                });
        }

        var tabClasses;

        function initTabs() {
            tabClasses = ["", ""];
        }

        this.getTabClass = function (tabNum) {
           return tabClasses[tabNum];
        };

        this.getTabPaneClass = function (tabNum) {
           return "tab-pane " + tabClasses[tabNum];
        };

        this.setActiveTab = function (tabNum) {
            initTabs();
            tabClasses[tabNum] = "active";
        };

        //Initialize 
        
        this.setActiveTab(1);
    }

})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['$log', '$q', 'UserService', 'ClientService', 'CardService', '$rootScope', '$scope', '$location', '$stateParams', '$state', '$filter', 'FlashService'];

    function RegisterController($log, $q, UserService, ClientService, CardService, $rootScope, $scope, $location, $stateParams, $state, $filter, FlashService) {
        var vm = this;
        //$log.info("Inside Register Controller");
        vm.user = {};
        vm.client = {};
        vm.client.names ={};
        vm.client.phones = [];
        vm.client.emailAddresses = [];
        vm.user.userType = 1;
        vm.register = register;
        vm.userLogin = {};
        vm.cardCode = {};
        vm.cardUpdateData = {};
        vm.card ={};
        
        function register() {
            //$log.info("Inside Register Function");
            var cardCode = "\""+$stateParams.id+"\"";
            if ($stateParams.id == undefined){
                vm.registerUser(vm.user);
            }else{
                //$log.info("The id to edit is: " + cardCode);
                var deferredCat = $q.defer();
                var getCardsPromise = deferredCat.promise;
                getCardsPromise = CardService.GetById(cardCode);
                getCardsPromise
                    .then(function (res) {
                        vm.card = res.data;
                        //$log.info("Card Data Response: " + JSON.stringify(vm.card));
                        if (vm.card == false) {
                            //FlashService.Error("Card Code Not Valid!");
                            //$log.info("Card Code Not Valid! " + JSON.stringify(res));
                            $location.path('/login');
                        } else {
                            if (vm.card.client_id === null) {
                                //$log.info("Card Data Registering: " + JSON.stringify(vm.card.client_id));
                                vm.registerUser(vm.user);
                            } else {
                                FlashService.Error("Card Data already Registered. Please Login!");
                                //$log.info("Card Data already Registered: " + JSON.stringify(vm.card.client_id));
                                $location.path('/clients/' + vm.card.client_id + '/public');
                            }
                        }
                    },
                        function (error) {
                            //$log.info(error);
                        },
                        function (progress) {
                            //$log.info(progress);
                        },
                        function () {
                            //$log.info("Nothing...");
                        });
            }
            vm.dataLoading = true;
        };
        
        this.getUuid = function uuidv4() {
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return v.toString(16);
            });
        };

        
        this.getCard = function () {
            var deferredCat = $q.defer();
            var getCardsPromise = deferredCat.promise;
            getCardsPromise = CardService.GetById("\""+$stateParams.id+"\"");
            getCardsPromise
                .then(function (res) {
                    if (res) {
                        vm.card = res.data;
                        //$log.info("Checking Card Use As? " + JSON.stringify(vm.card));                 
                            
                            if (vm.card == null) {
                                //FlashService.Error("Card Code Not Valid!");
                                //$log.info("Card Code Not Valid! " + JSON.stringify(res));
                               // $location.path('/login');
                            }else if(vm.card.useAs==1){
                                //$log.info("Card will be used a:" + vm.card.useAs);
                                //$log.info("Share your client details, ready to be saved instantly");
                            } else if (vm.card.useAs == 2){
                                //$log.info("Card will be used a:" + vm.card.useAs);
                                //$log.info("Share your degital business card with links to all your socail channel");
                                $location.path('/clients/' + vm.card.client_id + '/public');
                            } else if (vm.card.useAs == 3) {
                                //$log.info("Card will be used a:" + vm.card.useAs);
                                $location.path('/leads/' + vm.card.client_id + '/public');
                            }
                       
                    }
                },
                function (error) {
                    //$log.info(error);
                },
                function (progress) {
                    //$log.info(progress);
                },
                function () {
                    //$log.info("Nothing...");
                });
        };

        this.registerUser = function (user) {
            vm.user.registrationDate = new Date();
            //$log.info("Start To Create User");
            vm.userLogin.fullName = vm.user.firstName;//+ " "+user.lastName;
            vm.userLogin.email = vm.user.username;
            vm.userLogin.username = vm.user.username;
            vm.userLogin.avatar = "avatar";
            vm.userLogin.role = "Staff";
            vm.userLogin.status = "Active";
            vm.userLogin.pwd = vm.user.password;
            vm.userLogin.user_type = vm.user.userType;
            vm.userLogin.createdBy = "3";
            vm.userLogin.super_user = "1";
            //vm.userLogin.createdAt = vm.user.registrationDate;
            //$log.info("The UserLogin To Create : " + JSON.stringify(vm.userLogin));
            UserService.Create(vm.userLogin)
                .then(function (response) {
                    if (response.success) {
                        //$log.info("SUCCESS User Creation" + JSON.stringify(response));
                        vm.user.Created_By = response.data;
                        vm.registerClient(vm.user);
                        FlashService.Success('Registration successful!', true);
                    } else {
                        //$log.info("FAILED User Creation");
                        FlashService.Error(response.message);
                        vm.dataLoading = false;
                    }
                });
        };

        this.registerClient = function (user) {
            //$log.info("Last The user to creat Account : " + JSON.stringify(vm.userLogin));
            vm.client.names.first_name = vm.user.firstName;
            vm.client.names.middle_Name = vm.user.lastName;
            vm.client.names.last_Name = vm.user.lastName;
            //vm.client.Organization_1_Name = vm.user.companyName;
            vm.client.title = vm.user.title;
            
            var emailAddress = {};
            emailAddress.email_address = vm.user.username;
            emailAddress.label = "Primary";            
            vm.client.emailAddresses.push(emailAddress);

            var phone = {};
            phone.number = vm.user.phone;
            phone.label = "Primary";
            vm.client.phones.push(phone);
            vm.client.names.status = "Active";
            vm.client.names.user_id = user.Created_By
            vm.client.names.created_by = user.Created_By;

            //$log.info("Inside Create Client ");
            var deferredCat = $q.defer();
            var getClientsPromise = deferredCat.promise;
            //$log.info("Client Data to create: " + JSON.stringify(vm.client));
            getClientsPromise = ClientService.Update(vm.client);
            getClientsPromise
                .then(function (res) {
                    vm.client.id = res.data;
                    $log.info("Client Data Created: " + JSON.stringify(res));
                    if ($stateParams.id != undefined){
                        vm.UpdateCard(vm.client);
                        $log.info("Escape car update!");
                    }else{
                        $location.path('/login');
                        window.scrollTo(0, 0);
                    }
                                      
                },
                function (error) {
                    //$log.info(error);
                },
                function (progress) {
                    //$log.info(progress);
                },
                function () {
                    //$log.info("Nothing...");
                });
        };

        this.UpdateCard = function (client) {
            //$log.info("Inside Register Function");
            //$log.info("The id to edit is: " + $stateParams.id);
            var deferredCat = $q.defer();
            var getCardsPromise = deferredCat.promise;
            vm.cardUpdateData.id = $stateParams.id;
            vm.cardUpdateData.client_id = client.id;
            vm.cardUpdateData.Status = "Assigned";
            vm.cardUpdateData.useAs = "2";
            $log.info("Card Data Update Request: " + JSON.stringify(vm.cardUpdateData));
            getCardsPromise = CardService.Update(vm.cardUpdateData);
            getCardsPromise
                .then(function (res) {
                    //$log.info("Card Data Update Response: " + JSON.stringify(res));
                    $location.path('/login');
                    window.scrollTo(0, 0);           
                },
                function (error) {
                    //$log.info(error);
                },
                function (progress) {
                    //$log.info(progress);
                },
                function () {
                    //$log.info("Nothing...");
                });
        };
        this.getCard();
    }

})();

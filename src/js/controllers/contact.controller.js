/* globals confirm */
(function () {
    'use strict';
    //AlertService
    function ContactCtrl($log, $q, $scope, $rootScope, $location, $stateParams, $state, $filter, ContactService) {

        // this.AlertService = AlertService;

        // this.alerts = $rootScope.alerts;

        /**
         * Initial value of form
         *
         * @type {Array}
         */
        var vm = this;
        vm.social = {};
        vm.contact = [];
        vm.listContact = [];
        this.contactFromBC = [];
        this.itemsByPage = 15;
        vm.serviceUrl = $rootScope.globals.serviceUrl;
        /**
         * Reset the form values
         */

        this.reset = function () {
            this.contact = [
                {
                    id: '',
                    Name: '',
                    Given_Name: '',
                    Additional_Name: '',
                    Family_Name: '',
                    Phonetic_Name: '',
                    Given_Name_Phonetic: '',
                    Additional_Name_Phonetic: '',
                    Family_Name_Phonetic: '',
                    Name_Prefix: '',
                    Name_Suffix: '',
                    Initials: '',
                    Nickname: '',
                    Short_Name: '',
                    Maiden_Name: '',
                    Birthday: '',
                    Gender: '',
                    Location: '',
                    Billing_Information: '',
                    Directory_Server: '',
                    Mileage: '',
                    Occupation: '',
                    Hobby: '',
                    Sensitivity: '',
                    Priority: '',
                    Subject: '',
                    Notes: '',
                    Language: '',
                    Photo: '',
                    Group_Membership: '',
                    Email_1_Type: '',
                    Email_1_Value: '',
                    IM_1_Type: '',
                    IM_1_Service: '',
                    IM_1_Value: '',
                    Phone_1_Type: '',
                    Phone_1_Value: '',
                    Phone_2_Type: '',
                    Phone_2_Value: '',
                    Phone_3_Type: '',
                    Phone_3_Value: '',
                    Address_1_Type: '',
                    Address_1_Formatted: '',
                    Address_1_Street: '',
                    Address_1_City: '',
                    Address_1_PO_Box: '',
                    Address_1_Region: '',
                    Address_1_Postal_Code: '',
                    Address_1_Country: '',
                    Address_1_Extended_Address: '',
                    Organization_1_Type: '',
                    Organization_1_Name: '',
                    Organization_1_Phonetic_Name: '',
                    Organization_1_Title: '',
                    Organization_1_Department: '',
                    Organization_1_Symbol: '',
                    Organization_1_Location: '',
                    Organization_1_Job_Description: '',
                    Website_1_Type: '',
                    Website_1_Value: '',
                    Event_1_Type: '',
                    Event_1_Value: '',
                    Custom_Field_1_Type: '',
                    Custom_Field_1_Value: '',
                    Status: '',
                    Created_By: '',
                    Modified_By: '',
                    Created_At: '',
                    Modified_At: ''
                }
            ];
        };

        /**
         * Add a listContact in vm.listContact
         */
        this.create = function (contact) {
           ContactService.Create(contact);
            //   this.AlertService.add('success', 'Provider "' + contact.name + '" created with success!', 5000);
        };

        /**
         * Editing a individual contact
         * @return {[type]}     [description]
         */
        this.edit = function () {
            var id = $stateParams.id;
            //$log.info("The id to edit is: " + id);           
            var deferredCat = $q.defer();
            var getContactsPromise = deferredCat.promise;
            getContactsPromise = ContactService.GetById(id);
            getContactsPromise
                .then(function (res) {
                    vm.contact = res.data;
                    //$log.info("Contact Data: " + JSON.stringify(vm.contact));
                    /*if (vm.contact.Website_1_Value != null) {
                        var params = vm.contact.Website_1_Value.split(';');
                        for (var i = 0; i < params.length; i++) {
                            var parts = params[i].split('>');
                            vm.social[parts[0]] = parts[1];
                        }
                        //$log.info("Social Links: " + JSON.stringify(vm.social));
                    }*/
                },
                    function (error) {
                        //$log.info(error);
                    },
                    function (progress) {
                        //$log.info(progress);
                    },
                    function () {
                        //$log.info("Nothing...");
                    });
            window.scrollTo(0, 0);
        };
        /**
         * Update item
         * @param  {Object} item [description]
         * @return {[type]}      [description]
         */
        this.update = function (item) {
            //$log.info("The id to update is: " + JSON.stringify(item));

            vm.Contact = ContactService.Update(item);

            //   this.AlertService.add('success', 'Provider "' + item.name + '" updated with success!', 5000);
        };

        /**
         * Add/edit method abstration
         * @param  {Object} item [description]
         * @return {[type]}      [description]
         */
        this.save = function (item) {
            if (typeof item.id !== 'undefined') {
                this.update(item);
            } else {
                this.create(item);
            }
            this.reset();
            $location.path('/contacts');
        };

        /**
         * [delete description]
         * @param  {Integer} index        [description]
         * @param  {Boolean} confirmation [description]
         * @return {Boolean}              [description]
         */
        this.delete = function (index, confirmation) {
            confirmation = (typeof confirmation !== 'undefined') ? confirmation : true;
            if (confirmDelete(confirmation)) {
                var message,
                    item = ContactService.Delete(index);
                if (!!item) {
                    // message = 'Provider "' + item.name + '" was removed of your contact\'s list';
                    // this.AlertService.add('success', message, 5000);
                    vm.listContact = ContactService.GetAll();
                    return true;
                }
                //  this.AlertService.add('error', 'Houston, we have a problem. This operation cannot be executed correctly.', 5000);
                return false;
            }
        };

        /**
         * Method for access "window.confirm" function
         * @param  {Boolean} confirmation [description]
         * @return {Boolean}              [description]
         */
        var confirmDelete = function (confirmation) {
            return confirmation ? confirm('This action is irreversible. Do you want to delete this contact?') : true;
        };

        /**
         * Method for class initialization
         * @return {[type]} [description]
         */
        this.init = function () {            
            var deferredCat = $q.defer();
            var getContactsPromise = deferredCat.promise;
            getContactsPromise = ContactService.GetAll();
            getContactsPromise
                .then(function (res) {
                    if (res) {
                        vm.listContact = res.data;
                        //$log.info("Contacts Loaded" + vm.listContact.length);
                    }
                },
                    function (error) {
                        //$log.info(error);
                    },
                    function (progress) {
                        //$log.info(progress);
                    },
                    function () {
                        //$log.info("Nothing...");
                    });
        };
        //this.init();
        var tabClasses;

        function initTabs() {
            tabClasses = ["", ""];
        }

        this.getTabClass = function (tabNum) {
            return tabClasses[tabNum];
        };

        this.getTabPaneClass = function (tabNum) {
            return "tab-pane " + tabClasses[tabNum];
        }

        this.setActiveTab = function (tabNum) {
            initTabs();
            tabClasses[tabNum] = "active";
        };

        //Initialize 
        initTabs();
        this.setActiveTab(1);
    }

    angular.module('app')
        .controller('ContactCtrl', ContactCtrl);
    //'AlertService',
    ContactCtrl.$inject = ['$log', '$q', '$scope', '$rootScope', '$location', '$stateParams', '$state', '$filter', 'ContactService'];

}());

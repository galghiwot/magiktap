(function () {
    'use strict';

    angular
        .module('app')
        .controller('ProfileController', ProfileController);

    ProfileController.$inject = ['$log', '$q', 'UserService', '$rootScope', '$scope', '$location', '$stateParams', '$state', '$filter', 'ClientService'];

    function ProfileController($log, $q, UserService, $rootScope, $scope, $location, $stateParams, $state, $filter, ClientService) {
        var pc = this;
        pc.date = new Date();
        pc.user = null;
        pc.allUsers = [];
        pc.deleteUser = deleteUser;
        pc.client = {};

        initController();


        function initController() {
            loadCurrentUser();
            initTabs();
            //loadAllUsers();
        }

        function loadCurrentUser() {
            var deferredCat = $q.defer();
            var getClientsPromise = deferredCat.promise;
            getClientsPromise = ClientService.GetByUserId($rootScope.globals.currentUser.id);
            getClientsPromise
                .then(function (res) {
                    pc.client = res.data[0];
                },
                function (error) {
                    //$log.info(error);
                },
                function (progress) {
                    //$log.info(progress);
                },
                function () {
                    //$log.info("Nothing...");
                });
        }



        function loadAllUsers() {
            UserService.GetAll()
                .then(function (users) {
                    pc.allUsers = users;
                });
        }

        function deleteUser(id) {
            UserService.Delete(id)
                .then(function () {
                    loadAllUsers();
                });
        }

        var tabClasses;

        function initTabs() {
            tabClasses = ["", ""];
        }

        this.getTabClass = function (tabNum) {
            return tabClasses[tabNum];
        };

        this.getTabPaneClass = function (tabNum) {
            return "tab-pane " + tabClasses[tabNum];
        };

        this.setActiveTab = function (tabNum) {
            initTabs();
            tabClasses[tabNum] = "active";
        };

        //Initialize 

        this.setActiveTab(1);
    }

})();

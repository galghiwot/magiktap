angular.module('app', ['ngCookies', 'ui.bootstrap', 'ui.router', 'ngRoute', 'smart-table', 'chart.js', 'keepr', 'ngSanitize', 'ngCsv'])
.constant('CONFIG', {
    'API_URL': 'http://127.0.0.1/apps.magiktap/dist'
});
